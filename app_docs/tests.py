from django.test import TestCase, Client, RequestFactory
from app_signup import models as app_signup_models
from app_setup import models as app_setup_models
from app_docs import models as app_docs_models
from app_transaction import models as app_transaction_models
from app_docs import views as app_docs_views
from app_setup import views as app_setup_views
from django.contrib.sites.models import Site
from subdomains.utils import reverse as sub_reverse
from django.conf import settings
from fins.threadlocal import thread_local
from django.contrib.messages.storage.fallback import FallbackStorage
import psycopg2
import os
from app_auth.tests import load_url_pattern_names
from fins import urls  as fins_url
from fins.global_functions import get_db_name_test as get_db_name
import shutil




class DocsTest(TestCase):


    def setUp(self):
        self.client = Client()
        self.factory = RequestFactory()

        site = Site.objects.get(pk=1)
        site.domain = 'testserver'
        site.save()

        c = app_signup_models.Account(name='MyCompany', subdomain='mysubdomain')
        c.save()
        c.create_database()
        c.fill_database()


        user = app_signup_models.User(
            first_name='UserFirst',
            last_name='UserLast',
            email='b@b.ru')
        user.set_password('123')
        user.created_account_pk = c.pk
        user.save()
        user.account.add(c)

        unit1 = app_setup_models.Unit.objects.using(get_db_name()).all()[0]

        userdb = app_setup_models.DBUser.objects.using(get_db_name()).get(user_id=user.pk)
        userdb.access_app_docs = 2
        userdb.access_app_setup = 0
        userdb.unit = unit1
        userdb.save(using=get_db_name())

        self.my_user = user
        self.account = c


    def tearDown(self):

        #   we delete created account db
        connection_params = {
            'database': settings.MAIN_DB['NAME'],
            'host': settings.MAIN_DB['HOST'],
            'user': settings.MAIN_DB['USER'],
            'password': settings.MAIN_DB['PASSWORD']
        }
        connection = psycopg2.connect(**connection_params)
        connection.set_isolation_level(
            psycopg2.extensions.ISOLATION_LEVEL_AUTOCOMMIT
        )
        cursor = connection.cursor()

        for obj in app_signup_models.Account.objects.all():
            name = 'account_' + str(obj.pk)

            cursor.execute("SELECT pg_terminate_backend(pg_stat_activity.pid)"
                           " FROM pg_stat_activity"
                           " WHERE pg_stat_activity.datname = '%s' AND pid <> pg_backend_pid();" % name)
            cursor.execute('DROP DATABASE IF EXISTS "%s"' % name)

            media_folder = os.path.join(settings.MEDIA_URL, str(name))
            media_folder = media_folder.split("/", 1)[1]
            full_path = os.path.join(settings.BASE_DIR, media_folder)
            try:
                shutil.rmtree(full_path)
            except Exception:
                print('error in db deleting')



        connection.close()



    def test_agreement_CRUD(self):

        self.client.login(username='b@b.ru', password='123')

        # ========

        #create subcontractor
        link = sub_reverse('app_setup:subcontractor_create',subdomain='mysubdomain')
        data = {
                 'name': ['TestSub'],
                 'type': [1],
                 'taxe_type': [1],
                 'legal_form': [1],
                }
        request = self.factory.post(link, data)
        request.user = self.my_user
        request.account = self.account
        @thread_local(using_db=get_db_name())
        def tread(request):
            response = app_setup_views.subcontractor_create(request)
            return response
        response = tread(request)

        sub = app_transaction_models.Subcontractor.objects.using(get_db_name()).get(name = 'TestSub')


        # ========

        #create mycompany
        link = sub_reverse('app_setup:subcontractor_create',subdomain='mysubdomain')
        data = {
                 'name': ['TestMycompany'],
                 'type': [1],
                 'taxe_type': [1],
                 'legal_form': [1],
                }
        request = self.factory.post(link, data)
        request.user = self.my_user
        request.account = self.account
        @thread_local(using_db=get_db_name())
        def tread(request):
            response = app_setup_views.subcontractor_create(request)
            return response
        response = tread(request)

        mycompany = app_transaction_models.Subcontractor.objects.using(get_db_name()).get(name = 'TestMycompany')


        # ========

        #create
        link = sub_reverse('app_docs:agreement_create',subdomain='mysubdomain')
        data = {
                 'name': ['TestAgreement'],
                 'type': [0],
                 'prolongation_type': [2],
                 'status': [0],
                 'signed_at': ['12.01.2001'],
                 'subcontractor': sub.pk,
                 'subcontractor_role':['0'],
                 'mycompany': mycompany.pk,
                 'form-0-type':['0'],
                 'page':['null'],
                 'form-TOTAL_FORMS': ['1'],
                 'form-INITIAL_FORMS': ['0'],
                 'form-MIN_NUM_FORMS': ['0'],
                 'form-MAX_NUM_FORMS': ['1000'],
                }
        request = self.factory.post(link, data)
        request.user = self.my_user
        request.account = self.account
        @thread_local(using_db=get_db_name())
        def tread(request):
            response = app_docs_views.agreement_create(request)
            return response
        response = tread(request)

        created = app_docs_models.Agreement.objects.using(get_db_name()).get(name = 'TestAgreement')
        self.assertEqual(response.status_code, 200)
        self.assertTrue(created)
        self.assertEqual(len(app_docs_models.AgreementFile.objects.using(get_db_name()).all()), 0) # check that with no file attached there is no data added to model AgreementFile

        #update
        link = sub_reverse('app_docs:agreement_update', kwargs={'pk': created.pk} ,subdomain='mysubdomain')
        data = {
                 'name': ['TestAgreementEdit'],
                 'type': [0],
                 'prolongation_type': [2],
                 'status': [0],
                 'signed_at': ['12.01.2001'],
                 'subcontractor': sub.pk,
                 'subcontractor_role': ['0'],
                 'form-TOTAL_FORMS': ['1'],
                 'form-INITIAL_FORMS': ['0'],
                 'form-MIN_NUM_FORMS': ['0'],
                 'form-MAX_NUM_FORMS': ['1000'],
                'mycompany': mycompany.pk,
                'form-0-type': ['0'],
                'page': ['null'],
                }
        request = self.factory.post(link, data)
        request.user = self.my_user
        request.account = self.account
        @thread_local(using_db=get_db_name())
        def tread(request):
            response = app_docs_views.agreement_update(request, created.pk)
            return response

        #need this to make messages work
        setattr(request, 'session', 'session')
        messages = FallbackStorage(request)
        setattr(request, '_messages', messages)

        response = tread(request)

        updated = app_docs_models.Agreement.objects.using(get_db_name()).get(name = 'TestAgreementEdit')
        self.assertEqual(response.status_code, 200)
        self.assertTrue(updated)

        #delete
        link = sub_reverse('app_docs:agreement_delete',kwargs={'pk': created.pk}, subdomain='mysubdomain')
        data = {}
        request = self.factory.post(link, data)
        request.user = self.my_user
        request.account = self.account
        @thread_local(using_db=get_db_name())
        def tread(request):
            response = app_docs_views.agreement_delete(request, created.pk)
            return response
        #need this to make messages work
        setattr(request, 'session', 'session')
        messages = FallbackStorage(request)
        setattr(request, '_messages', messages)
        response = tread(request)

        deleted = app_docs_models.Agreement.objects.using(get_db_name()).filter(name = 'TestAgreement')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(deleted), 0)


        # ========

        #check that if we add comments but wouldn't attach file there will be no data added to model AgreementFile
        link = sub_reverse('app_docs:agreement_create',subdomain='mysubdomain')
        data = {
                 'name': ['TestAgreement'],
                 'type': [0],
                 'prolongation_type': [2],
                 'status': [0],
                 'subcontractor': sub.pk,
                 'signed_at': ['12.01.2001'],
                 'subcontractor_role':['0'],
                 'form-TOTAL_FORMS': ['1'],
                 'form-INITIAL_FORMS': ['0'],
                 'form-MIN_NUM_FORMS': ['0'],
                 'form-MAX_NUM_FORMS': ['1000'],
                 'form-0-comment': ['test'],
                    'mycompany': mycompany.pk,
                    'form-0-type': ['0'],
                    'page': ['null'],
                }
        request = self.factory.post(link, data)
        request.user = self.my_user
        request.account = self.account
        @thread_local(using_db=get_db_name())
        def tread(request):
            response = app_docs_views.agreement_create(request)
            return response
        response = tread(request)

        self.assertEqual(len(app_docs_models.AgreementFile.objects.using(get_db_name()).all()), 0)

    def test_agreement_file_CRUD(self):

        self.client.login(username='b@b.ru', password='123')

        # ========

        #create subcontractor
        link = sub_reverse('app_setup:subcontractor_create',subdomain='mysubdomain')
        data = {
                 'name': ['TestSub'],
                 'type': [1],
                 'taxe_type': [1],
                 'legal_form': [1],
                }
        request = self.factory.post(link, data)
        request.user = self.my_user
        request.account = self.account
        @thread_local(using_db=get_db_name())
        def tread(request):
            response = app_setup_views.subcontractor_create(request)
            return response
        response = tread(request)

        sub = app_transaction_models.Subcontractor.objects.using(get_db_name()).get(name = 'TestSub')

        # ========

        #create mycompany
        link = sub_reverse('app_setup:subcontractor_create',subdomain='mysubdomain')
        data = {
                 'name': ['TestMycompany'],
                 'type': [1],
                 'taxe_type': [1],
                 'legal_form': [1],
                }
        request = self.factory.post(link, data)
        request.user = self.my_user
        request.account = self.account
        @thread_local(using_db=get_db_name())
        def tread(request):
            response = app_setup_views.subcontractor_create(request)
            return response
        response = tread(request)

        mycompany = app_transaction_models.Subcontractor.objects.using(get_db_name()).get(name = 'TestMycompany')


        # ========

        #create agreement and attach file
        path = os.path.join(settings.BASE_DIR, 'app_docs/views/main.py' )
        myfile = open(path, 'r')

        link = sub_reverse('app_docs:agreement_create',subdomain='mysubdomain')
        data = {
                 'name': ['TestAgreement'],
                 'type': [0],
                 'prolongation_type': [2],
                 'status': [0],
                 'signed_at': ['12.01.2001'],
                 'subcontractor': sub.pk,
                 'subcontractor_role':['0'],
                 'form-TOTAL_FORMS': ['1'],
                 'form-INITIAL_FORMS': ['0'],
                 'form-MIN_NUM_FORMS': ['0'],
                 'form-MAX_NUM_FORMS': ['1000'],
                 'form-0-file': myfile,
                'mycompany': mycompany.pk,
                'form-0-type': ['0'],
                'page': ['null'],
                }
        request = self.factory.post(link, data)
        request.user = self.my_user
        request.account = self.account
        @thread_local(using_db=get_db_name())
        def tread(request):
            response = app_docs_views.agreement_create(request)
            return response
        response = tread(request)

        # check that file is attached to the agreement
        created = app_docs_models.Agreement.objects.using(get_db_name()).get(name = 'TestAgreement')
        file = app_docs_models.AgreementFile.objects.using(get_db_name()).get(agreement=created.pk)
        self.assertTrue(file)
        self.assertEqual(file.comment, '')

        file_path = os.path.join(settings.MEDIA_ROOT, str(file.file) )
        print('file_path', file_path)
        try:
            open(file_path, 'r')
            exist = True
        except Exception:
            exist = False
        self.assertTrue(exist)






        #update file info
        link = sub_reverse('app_docs:agreement_update', kwargs={'pk': created.pk} ,subdomain='mysubdomain')
        data = {
                 'name': ['TestAgreementEdit'],
                 'type': [0],
                 'prolongation_type': [2],
                 'status': [0],
                 'signed_at': ['12.01.2001'],
                 'subcontractor': sub.pk,
                 'subcontractor_role': ['0'],
                 'form-TOTAL_FORMS': ['2'],
                 'form-INITIAL_FORMS': ['1'],
                 'form-MIN_NUM_FORMS': ['0'],
                 'form-MAX_NUM_FORMS': ['1000'],
                 'form-0-id': created.pk,
                 'form-0-comment': 'test_comment',
            'mycompany': mycompany.pk,
            'form-0-type': ['0'],
            'form-1-type': ['0'],
            'page': ['null'],
                }
        request = self.factory.post(link, data)
        request.user = self.my_user
        request.account = self.account
        @thread_local(using_db=get_db_name())
        def tread(request):
            response = app_docs_views.agreement_update(request, created.pk)
            return response

        #need this to make messages work
        setattr(request, 'session', 'session')
        messages = FallbackStorage(request)
        setattr(request, '_messages', messages)

        tread(request)

        # check that comment is saved
        updated = app_docs_models.Agreement.objects.using(get_db_name()).get(name = 'TestAgreementEdit')
        file = app_docs_models.AgreementFile.objects.using(get_db_name()).get(agreement=updated.pk)
        self.assertEqual(file.comment, 'test_comment')








        #delete file
        link = sub_reverse('app_docs:agreement_update', kwargs={'pk': created.pk}, subdomain='mysubdomain')
        data = {
            'name': ['TestAgreementEdit'],
            'type': [0],
            'prolongation_type': [2],
            'status': [0],
            'subcontractor': sub.pk,
            'subcontractor_role': ['0'],
            'signed_at': ['12.01.2001'],
            'form-TOTAL_FORMS': ['2'],
            'form-INITIAL_FORMS': ['1'],
            'form-MIN_NUM_FORMS': ['0'],
            'form-MAX_NUM_FORMS': ['1000'],
            'form-0-id': created.pk,
            'form-0-comment': 'test_comment',
            'form-0-DELETE': ['on'],
            'mycompany': mycompany.pk,
            'form-0-type': ['0'],
            'form-1-type': ['0'],
            'page': ['null'],
        }
        request = self.factory.post(link, data)
        request.user = self.my_user
        request.account = self.account

        @thread_local(using_db=get_db_name())
        def tread(request):
            response = app_docs_views.agreement_update(request, created.pk)
            return response
        tread(request)


        # check that comment is saved
        files = app_docs_models.AgreementFile.objects.using(get_db_name()).all()
        self.assertEqual(len(files), 0)



        try:
            open(file_path, 'r')
            exist = True
        except Exception:
            exist = False
        self.assertFalse(exist)

    def test_agreement_parent_defines_sub(self):
        '''
        we test that subcontractor and its role is defined by parent
        '''


        self.client.login(username='b@b.ru', password='123')

        # ========

        #create 2 subcontractors
        link = sub_reverse('app_setup:subcontractor_create',subdomain='mysubdomain')
        data = {
                 'name': ['TestSub'],
                 'type': [1],
                 'taxe_type': [1],
                 'legal_form': [1],
                }
        request = self.factory.post(link, data)
        request.user = self.my_user
        request.account = self.account
        @thread_local(using_db=get_db_name())
        def tread(request):
            response = app_setup_views.subcontractor_create(request)
            return response
        tread(request)

        data = {
            'name': ['TestSub2'],
            'type': [1],
            'taxe_type': [1],
            'legal_form': [1],
        }
        request = self.factory.post(link, data)
        request.user = self.my_user
        request.account = self.account
        @thread_local(using_db=get_db_name())
        def tread(request):
            response = app_setup_views.subcontractor_create(request)
            return response
        tread(request)

        sub = app_transaction_models.Subcontractor.objects.using(get_db_name()).get(name = 'TestSub')
        sub2 = app_transaction_models.Subcontractor.objects.using(get_db_name()).get(name='TestSub2')

        # ========

        #create mycompany
        link = sub_reverse('app_setup:subcontractor_create',subdomain='mysubdomain')
        data = {
                 'name': ['TestMycompany'],
                 'type': [1],
                 'taxe_type': [1],
                 'legal_form': [1],
                }
        request = self.factory.post(link, data)
        request.user = self.my_user
        request.account = self.account
        @thread_local(using_db=get_db_name())
        def tread(request):
            response = app_setup_views.subcontractor_create(request)
            return response
        response = tread(request)

        mycompany = app_transaction_models.Subcontractor.objects.using(get_db_name()).get(name = 'TestMycompany')


        # ========

        #create 2 argreements
        link = sub_reverse('app_docs:agreement_create',subdomain='mysubdomain')
        data = {
                 'name': ['TestAgreement'],
                 'type': [0],
                 'number': [1],
                 'signed_at': ['12.12.2009'],
                 'prolongation_type': [2],
                 'status': [0],
                 'subcontractor': sub.pk,
                 'subcontractor_role':['0'],
                 'form-TOTAL_FORMS': ['1'],
                 'form-INITIAL_FORMS': ['0'],
                 'form-MIN_NUM_FORMS': ['0'],
                 'form-MAX_NUM_FORMS': ['1000'],
            'mycompany': mycompany.pk,
            'form-0-type': ['0'],
            'page': ['null'],
                }
        request = self.factory.post(link, data)
        request.user = self.my_user
        request.account = self.account
        @thread_local(using_db=get_db_name())
        def tread(request):
            response = app_docs_views.agreement_create(request)
            return response
        tread(request)


        data = {
            'name': ['TestAgreement2'],
            'type': [0],
            'number': [2],
            'signed_at': ['12.12.2009'],
            'prolongation_type': [2],
            'status': [0],
            'subcontractor': sub2.pk,
            'subcontractor_role': ['1'],
            'form-TOTAL_FORMS': ['1'],
            'form-INITIAL_FORMS': ['0'],
            'form-MIN_NUM_FORMS': ['0'],
            'form-MAX_NUM_FORMS': ['1000'],
            'mycompany': mycompany.pk,
            'form-0-type': ['0'],
            'page': ['null'],
        }
        request = self.factory.post(link, data)
        request.user = self.my_user
        request.account = self.account

        @thread_local(using_db=get_db_name())
        def tread(request):
            response = app_docs_views.agreement_create(request)
            return response
        tread(request)

        created = app_docs_models.Agreement.objects.using(get_db_name()).get(name = 'TestAgreement')
        created2 = app_docs_models.Agreement.objects.using(get_db_name()).get(name='TestAgreement2')

        self.assertEqual(created.subcontractor, sub)
        self.assertEqual(created.subcontractor_role, 0)
        self.assertEqual(created2.subcontractor, sub2)
        self.assertEqual(created2.subcontractor_role, 1)

        #update argeement #2 - make it child to agrement 1. check if sub and sub role had changed
        link = sub_reverse('app_docs:agreement_update', kwargs={'pk': created2.pk} ,subdomain='mysubdomain')
        data = {
            'name': ['TestAgreement2'],
            'type': [0],
            'number': [2],
            'signed_at': ['12.12.2009'],
            'prolongation_type': [2],
            'parent': [created.pk],
            'status': [0],
            'subcontractor': sub2.pk,
            'subcontractor_role': ['1'],
            'form-TOTAL_FORMS': ['1'],
            'form-INITIAL_FORMS': ['0'],
            'form-MIN_NUM_FORMS': ['0'],
            'form-MAX_NUM_FORMS': ['1000'],
            'mycompany': mycompany.pk,
            'form-0-type': ['0'],
            'page': ['null'],
        }
        request = self.factory.post(link, data)
        request.user = self.my_user
        request.account = self.account
        @thread_local(using_db=get_db_name())
        def tread(request):
            response = app_docs_views.agreement_update(request, created2.pk)
            return response
        tread(request)

        updated = app_docs_models.Agreement.objects.using(get_db_name()).get(name = 'TestAgreement2')

        self.assertEqual(updated.subcontractor, created.subcontractor)
        self.assertEqual(updated.subcontractor_role, created.subcontractor_role)

    def test_agreement_docgen(self):

        """
        only checks that docs is generated even with poor data
        :return:
        """

        self.client.login(username='b@b.ru', password='123')

        # ========

        #create subcontractor
        link = sub_reverse('app_setup:subcontractor_create',subdomain='mysubdomain')
        data = {
                 'name': ['TestSub'],
                 'type': [1],
                 'taxe_type': [1],
                 'legal_form': [1],
                }
        request = self.factory.post(link, data)
        request.user = self.my_user
        request.account = self.account
        @thread_local(using_db=get_db_name())
        def tread(request):
            response = app_setup_views.subcontractor_create(request)
            return response
        response = tread(request)

        sub = app_transaction_models.Subcontractor.objects.using(get_db_name()).get(name = 'TestSub')


        # ========

        #create mycompany
        link = sub_reverse('app_setup:subcontractor_create',subdomain='mysubdomain')
        data = {
                 'name': ['TestMycompany'],
                 'type': [1],
                 'taxe_type': [1],
                 'legal_form': [1],
                }
        request = self.factory.post(link, data)
        request.user = self.my_user
        request.account = self.account
        @thread_local(using_db=get_db_name())
        def tread(request):
            response = app_setup_views.subcontractor_create(request)
            return response
        response = tread(request)

        mycompany = app_transaction_models.Subcontractor.objects.using(get_db_name()).get(name = 'TestMycompany')


        # ========

        path = os.path.join(settings.BASE_DIR, 'fins/static/docs/template_for_test.docx' )
        myfile = open(path, 'rb')

        #create doc template
        link = sub_reverse('app_setup:doc_template_create',subdomain='mysubdomain')
        data = {
                 'name': ['TestTemplate'],
                 'file': myfile,
                }
        request = self.factory.post(link, data)
        request.user = self.my_user
        request.account = self.account
        @thread_local(using_db=get_db_name())
        def tread(request):
            response = app_setup_views.doc_template_create(request)
            return response


        #need this to make messages work
        setattr(request, 'session', 'session')
        messages = FallbackStorage(request)
        setattr(request, '_messages', messages)

        response = tread(request)

        template_created = app_docs_models.DocTemplateFile.objects.using(get_db_name()).get(name = 'TestTemplate')

        self.assertTrue(template_created)


        file_path = os.path.join(settings.MEDIA_ROOT, str(template_created.file) )
        try:
            open(file_path, 'r')
            exist = True
        except Exception:
            exist = False
        self.assertTrue(exist)


        # ========

        #create argeement with doc template
        link = sub_reverse('app_docs:agreement_create',subdomain='mysubdomain')
        data = {
                 'name': ['TestAgreement'],
                 'type': [0],
                 'prolongation_type': [2],
                 'status': [0],
                 'signed_at': ['12.01.2001'],
                 'subcontractor': sub.pk,
                 'subcontractor_role':['0'],
                 'mycompany': mycompany.pk,
                 'doc_template': template_created.pk,
                 'form-0-type':['0'],
                 'page':['null'],
                 'form-TOTAL_FORMS': ['1'],
                 'form-INITIAL_FORMS': ['0'],
                 'form-MIN_NUM_FORMS': ['0'],
                 'form-MAX_NUM_FORMS': ['1000'],
                }
        request = self.factory.post(link, data)
        request.user = self.my_user
        request.account = self.account
        @thread_local(using_db=get_db_name())
        def tread(request):
            response = app_docs_views.agreement_create(request)
            return response
        response = tread(request)

        created = app_docs_models.Agreement.objects.using(get_db_name()).get(name = 'TestAgreement')

        # ============
        # gen template

        link = sub_reverse('app_docs:doc_generate', kwargs={'pk': created.pk, 'type': '0'}, subdomain='mysubdomain')
        data = {
        }
        request = self.factory.post(link, data)
        request.user = self.my_user
        request.account = self.account

        @thread_local(using_db=get_db_name())
        def tread(request):
            response = app_docs_views.doc_generate(request, created.pk, '0' )
            return response

        response = tread(request)
        self.assertEqual(response.status_code, 200)




    def test_accounting_docs_CRUD(self):

        self.client.login(username='b@b.ru', password='123')

        # ========

        # create subcontractor
        link = sub_reverse('app_setup:subcontractor_create', subdomain='mysubdomain')
        data = {
            'name': ['TestSub'],
            'type': [1],
            'taxe_type': [1],
            'legal_form': [1],
        }
        request = self.factory.post(link, data)
        request.user = self.my_user
        request.account = self.account

        @thread_local(using_db=get_db_name())
        def tread(request):
            response = app_setup_views.subcontractor_create(request)
            return response

        response = tread(request)
        sub = app_transaction_models.Subcontractor.objects.using(get_db_name()).get(name='TestSub')

        # ========

        #create mycompany
        link = sub_reverse('app_setup:subcontractor_create',subdomain='mysubdomain')
        data = {
                 'name': ['TestMycompany'],
                 'type': [1],
                 'taxe_type': [1],
                 'legal_form': [1],
                }
        request = self.factory.post(link, data)
        request.user = self.my_user
        request.account = self.account
        @thread_local(using_db=get_db_name())
        def tread(request):
            response = app_setup_views.subcontractor_create(request)
            return response
        response = tread(request)

        mycompany = app_transaction_models.Subcontractor.objects.using(get_db_name()).get(name = 'TestMycompany')

        # ========

        # create agreement
        link = sub_reverse('app_docs:agreement_create', subdomain='mysubdomain')
        data = {
            'name': ['TestAgreement'],
            'type': [0],
            'prolongation_type': [2],
            'status': [0],
            'signed_at': ['12.01.2001'],
            'subcontractor': sub.pk,
            'subcontractor_role': ['0'],
            'form-TOTAL_FORMS': ['1'],
            'form-INITIAL_FORMS': ['0'],
            'form-MIN_NUM_FORMS': ['0'],
            'form-MAX_NUM_FORMS': ['1000'],
            'form-0-type': ['0'],
            'mycompany': mycompany.pk,
            'page': ['null'],
        }
        request = self.factory.post(link, data)
        request.user = self.my_user
        request.account = self.account

        @thread_local(using_db=get_db_name())
        def tread(request):
            response = app_docs_views.agreement_create(request)
            return response

        response = tread(request)

        agreement = app_docs_models.Agreement.objects.using(get_db_name()).get(name='TestAgreement')

        #create ad
        link = sub_reverse('app_docs:accounting_docs_create',subdomain='mysubdomain')
        data = {
                'name': ['123'],
                'type': [0],
                'status': [0],
                'signed_at': ['12.12.2020'],
                'agreement': [agreement.pk],
                'form-TOTAL_FORMS': ['1'],
                'form-INITIAL_FORMS': ['0'],
                'form-MIN_NUM_FORMS': ['0'],
                'form-MAX_NUM_FORMS': ['1000'],
            'form-0-type': ['0'],
                }
        request = self.factory.post(link, data)
        request.user = self.my_user
        request.account = self.account
        @thread_local(using_db=get_db_name())
        def tread(request):
            response = app_docs_views.accounting_docs_create(request)
            return response
        response = tread(request)

        created = app_docs_models.AccountingDocs.objects.using(get_db_name()).get(name = '123')
        self.assertEqual(response.status_code, 200)
        self.assertTrue(created)

        #update
        link = sub_reverse('app_docs:accounting_docs_update', kwargs={'pk': created.pk} ,subdomain='mysubdomain')
        data = {
                'name': ['12345'],
                'type': [0],
                'status': [0],
                'signed_at': ['12.12.2020'],
                'agreement': [agreement.pk],
                'form-TOTAL_FORMS': ['1'],
                'form-INITIAL_FORMS': ['0'],
                'form-MIN_NUM_FORMS': ['0'],
                'form-MAX_NUM_FORMS': ['1000'],
            'form-0-type': ['0'],
                }
        request = self.factory.post(link, data)
        request.user = self.my_user
        request.account = self.account
        @thread_local(using_db=get_db_name())
        def tread(request):
            response = app_docs_views.accounting_docs_update(request, created.pk)
            return response

        #need this to make messages work
        setattr(request, 'session', 'session')
        messages = FallbackStorage(request)
        setattr(request, '_messages', messages)

        response = tread(request)

        updated = app_docs_models.AccountingDocs.objects.using(get_db_name()).get(name = '12345')
        self.assertEqual(response.status_code, 200)
        self.assertTrue(updated)

        #delete
        link = sub_reverse('app_docs:accounting_docs_delete',kwargs={'pk': created.pk}, subdomain='mysubdomain')
        data = {}
        request = self.factory.post(link, data)
        request.user = self.my_user
        request.account = self.account
        @thread_local(using_db=get_db_name())
        def tread(request):
            response = app_docs_views.accounting_docs_delete(request, created.pk)
            return response
        #need this to make messages work
        setattr(request, 'session', 'session')
        messages = FallbackStorage(request)
        setattr(request, '_messages', messages)
        response = tread(request)

        deleted = app_docs_models.AccountingDocs.objects.using(get_db_name()).filter(name = '123')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(deleted), 0)

    def test_accounting_docs_file_CRUD(self):

        self.client.login(username='b@b.ru', password='123')

        # ========

        # create subcontractor
        link = sub_reverse('app_setup:subcontractor_create', subdomain='mysubdomain')
        data = {
            'name': ['TestSub'],
            'type': [1],
            'taxe_type': [1],
            'legal_form': [1],
        }
        request = self.factory.post(link, data)
        request.user = self.my_user
        request.account = self.account

        @thread_local(using_db=get_db_name())
        def tread(request):
            response = app_setup_views.subcontractor_create(request)
            return response

        response = tread(request)

        sub = app_transaction_models.Subcontractor.objects.using(get_db_name()).get(name='TestSub')


        # ========

        #create mycompany
        link = sub_reverse('app_setup:subcontractor_create',subdomain='mysubdomain')
        data = {
                 'name': ['TestMycompany'],
                 'type': [1],
                 'taxe_type': [1],
                 'legal_form': [1],
                }
        request = self.factory.post(link, data)
        request.user = self.my_user
        request.account = self.account
        @thread_local(using_db=get_db_name())
        def tread(request):
            response = app_setup_views.subcontractor_create(request)
            return response
        response = tread(request)

        mycompany = app_transaction_models.Subcontractor.objects.using(get_db_name()).get(name = 'TestMycompany')


        # ========

        # create agreement
        link = sub_reverse('app_docs:agreement_create', subdomain='mysubdomain')
        data = {
            'name': ['TestAgreement'],
            'type': [0],
            'prolongation_type': [2],
            'status': [0],
            'subcontractor': sub.pk,
            'signed_at': ['12.09.1990'],
            'subcontractor_role': ['0'],
            'form-TOTAL_FORMS': ['1'],
            'form-INITIAL_FORMS': ['0'],
            'form-MIN_NUM_FORMS': ['0'],
            'form-MAX_NUM_FORMS': ['1000'],
            'mycompany': mycompany.pk,
            'form-0-type': ['0'],
            'page': ['null'],
        }
        request = self.factory.post(link, data)
        request.user = self.my_user
        request.account = self.account

        @thread_local(using_db=get_db_name())
        def tread(request):
            response = app_docs_views.agreement_create(request)
            return response

        tread(request)

        agreement = app_docs_models.Agreement.objects.using(get_db_name()).get(name='TestAgreement')



        # create accounting docs and attach file
        path = os.path.join(settings.BASE_DIR, 'app_docs/views/main.py')
        myfile = open(path, 'r')

        link = sub_reverse('app_docs:accounting_docs_create',subdomain='mysubdomain')
        data = {
                 'name': ['123'],
                 'type': [0],
                 'status': [0],
                 'signed_at': ['12.09.1990'],
                 'agreement': agreement.pk,
                'form-TOTAL_FORMS': ['1'],
                'form-INITIAL_FORMS': ['0'],
                'form-MIN_NUM_FORMS': ['0'],
                'form-MAX_NUM_FORMS': ['1000'],
                'form-0-file': myfile,
                'form-0-type': ['0'],

                }
        request = self.factory.post(link, data)
        request.user = self.my_user
        request.account = self.account
        @thread_local(using_db=get_db_name())
        def tread(request):
            response = app_docs_views.accounting_docs_create(request)
            return response
        response = tread(request)

        # check that file is attached to the accounting doc

        created = app_docs_models.AccountingDocs.objects.using(get_db_name()).get(name = '123')
        file = app_docs_models.ADFile.objects.using(get_db_name()).get(ad=created.pk)
        self.assertTrue(file)
        self.assertEqual(file.comment, '')
        file_path = os.path.join(settings.MEDIA_ROOT, str(file.file))
        try:
            open(file_path, 'r')
            exist = True
        except Exception:
            exist = False
        self.assertTrue(exist)


        #update
        link = sub_reverse('app_docs:accounting_docs_update', kwargs={'pk': created.pk} ,subdomain='mysubdomain')
        data = {
            'name': ['12345'],
            'type': [0],
            'status': [0],
            'signed_at': ['12.09.1990'],
            'agreement': agreement.pk,
            'form-TOTAL_FORMS': ['2'],
            'form-INITIAL_FORMS': ['1'],
            'form-MIN_NUM_FORMS': ['0'],
            'form-MAX_NUM_FORMS': ['1000'],
            'form-0-id': created.pk,
            'form-0-comment': 'test_comment',
            'form-0-type': ['0'],
            'form-1-type': ['0'],
                }
        request = self.factory.post(link, data)
        request.user = self.my_user
        request.account = self.account
        @thread_local(using_db=get_db_name())
        def tread(request):
            response = app_docs_views.accounting_docs_update(request, created.pk)
            return response
        tread(request)

        # check that comment is saved
        updated = app_docs_models.AccountingDocs.objects.using(get_db_name()).get(name = '12345')
        file = app_docs_models.ADFile.objects.using(get_db_name()).get(ad=updated.pk)
        self.assertEqual(file.comment, 'test_comment')

        # delete file
        link = sub_reverse('app_docs:accounting_docs_update', kwargs={'pk': created.pk}, subdomain='mysubdomain')
        data = {
            'name': ['12345'],
            'type': [0],
            'status': [0],
            'signed_at': ['12.09.1990'],
            'agreement': agreement.pk,
            'form-TOTAL_FORMS': ['2'],
            'form-INITIAL_FORMS': ['1'],
            'form-MIN_NUM_FORMS': ['0'],
            'form-MAX_NUM_FORMS': ['1000'],
            'form-0-id': created.pk,
            'form-0-comment': 'test_comment',
            'form-0-DELETE': ['on'],
            'form-0-type': ['0'],
            'form-1-type': ['0'],
        }
        request = self.factory.post(link, data)
        request.user = self.my_user
        request.account = self.account

        @thread_local(using_db=get_db_name())
        def tread(request):
            response = app_docs_views.accounting_docs_update(request, created.pk)
            return response
        tread(request)

        # check that comment is saved
        files = app_docs_models.ADFile.objects.using(get_db_name()).all()
        self.assertEqual(len(files), 0)
        #
        # try:
        #     open(file_path, 'r')
        #     exist = True
        # except Exception:
        #     exist = False
        # self.assertFalse(exist)

    def test_lister(self):


        self.client.login(username='b@b.ru', password='123')
        # ========

        #create subcontractor
        link = sub_reverse('app_setup:subcontractor_create',subdomain='mysubdomain')
        data = {
                 'name': ['TestSub'],
                 'type': [1],
                 'taxe_type': [1],
                 'legal_form': [1],
                }
        request = self.factory.post(link, data)
        request.user = self.my_user
        request.account = self.account
        @thread_local(using_db=get_db_name())
        def tread(request):
            response = app_setup_views.subcontractor_create(request)
            return response
        response = tread(request)

        sub = app_transaction_models.Subcontractor.objects.using(get_db_name()).get(name = 'TestSub')


        # ========

        #create mycompany
        link = sub_reverse('app_setup:subcontractor_create',subdomain='mysubdomain')
        data = {
                 'name': ['TestMycompany'],
                 'type': [1],
                 'taxe_type': [1],
                 'legal_form': [1],
                }
        request = self.factory.post(link, data)
        request.user = self.my_user
        request.account = self.account
        @thread_local(using_db=get_db_name())
        def tread(request):
            response = app_setup_views.subcontractor_create(request)
            return response
        response = tread(request)

        mycompany = app_transaction_models.Subcontractor.objects.using(get_db_name()).get(name = 'TestMycompany')


        # ========

        #create agreement level 0
        a = app_docs_models.Agreement(
            name='1_level_0',
            type=0,
            status=0,
            subcontractor=sub,
            mycompany=mycompany,
            signed_at='2000-12-12',
        )
        a.save(using=get_db_name())

        #create agreement level 0
        b = app_docs_models.Agreement(
            name='2_level_0',
            type=0,
            status=0,
            subcontractor=sub,
            mycompany=mycompany,
            signed_at='2000-12-12',
        )
        b.save(using=get_db_name())

        #create agreement level 1
        c = app_docs_models.Agreement(
            name='3_level_1_parent_1',
            type=0,
            status=0,
            parent=a.pk,
            subcontractor=sub,
            mycompany=mycompany,
            signed_at='2000-12-12',
        )
        c.save(using=get_db_name())


        #create agreement level 1
        d = app_docs_models.Agreement(
            name='4_level_1_parent_2',
            type=0,
            status=0,
            parent=b.pk,
            subcontractor=sub,
            mycompany=mycompany,
            signed_at='2000-12-12',
        )
        d.save(using=get_db_name())

        #create agreement level 2
        e = app_docs_models.Agreement(
            name='5_level_2_parent_4',
            type=0,
            status=0,
            parent=c.pk,
            subcontractor=sub,
            mycompany=mycompany,
            signed_at='2000-12-12',
        )
        e.save(using=get_db_name())


        self.assertTrue(a)
        self.assertTrue(b)
        self.assertTrue(c)
        self.assertTrue(d)
        self.assertTrue(e)


        lister = app_docs_views.lister([], app_docs_models.Agreement.objects.using(get_db_name()).all())
        etalon = [a, c, e, b, d]
        self.assertEqual(lister, etalon)

        #tests infinite recursion
        lister = app_docs_views.lister([], app_docs_models.Agreement.objects.using(get_db_name()).filter(pk=e.pk))
        etalon = [e]
        self.assertEqual(lister, etalon)

        lister = app_docs_views.lister([], app_docs_models.Agreement.objects.using(get_db_name()).filter(pk__in=[a.pk, b.pk, e.pk]))
        etalon = [a,b,e]
        self.assertEqual(lister, etalon)



class AccessPermissionsTest(TestCase):


    def setUp(self):
        self.client = Client()
        self.factory = RequestFactory()

        site = Site.objects.get(pk=1)
        site.domain = 'testserver'
        site.save()

        c = app_signup_models.Account(name='MyCompany', subdomain='mysubdomain')
        c.save()
        c.create_database()
        c.fill_database()

        user = app_signup_models.User(
            first_name='UserFirst',
            last_name='UserLast',
            email='b@b.ru')
        user.set_password('123')
        user.created_account_pk = c.pk
        user.save()
        user.account.add(c)


        self.my_user = user
        self.account = c
        self.dbuser = app_setup_models.DBUser.objects.using(get_db_name()).get(
            user_id=user.pk)

        #fill db ... stupidly)

        self.client.login(username='b@b.ru', password='123')
        link = sub_reverse('app_dashboard:index',subdomain='mysubdomain')
        request = self.factory.get(link)
        request.user = self.my_user
        request.account = self.account
        db_name_1 = get_db_name()

        @thread_local(using_db=db_name_1)
        def tread(request):
            response = app_setup_views.subcontractor_create(request)
            return response

    def tearDown(self):

        #   we delete created account db
        connection_params = {
            'database': settings.MAIN_DB['NAME'],
            'host': settings.MAIN_DB['HOST'],
            'user': settings.MAIN_DB['USER'],
            'password': settings.MAIN_DB['PASSWORD']
        }
        connection = psycopg2.connect(**connection_params)
        connection.set_isolation_level(
            psycopg2.extensions.ISOLATION_LEVEL_AUTOCOMMIT
        )
        cursor = connection.cursor()

        for obj in app_signup_models.Account.objects.all():
            name = 'account_' + str(obj.pk)

            cursor.execute("SELECT pg_terminate_backend(pg_stat_activity.pid)"
                           " FROM pg_stat_activity"
                           " WHERE pg_stat_activity.datname = '%s' AND pid <> pg_backend_pid();" % name)
            cursor.execute('DROP DATABASE IF EXISTS "%s"' % name)
        connection.close()

    def test_access_permission(self):
        """
        test app_setup access and redirect if denied
        Exceptions are given in exception list

        """

        Exeption_app_list_default = ['admin', ]
        Exeption_app_list_no_login = ['app_about', 'app_auth', 'app_signup',]
        Exeption_name_list_default = []
        Exeption_name_list_no_login = ['agreement_create', 'agreement_save', 'agreement_update', 'agreement_delete',
                                       'accounting_docs_create', 'accounting_docs_save', 'accounting_docs_update', 'accounting_docs_delete',
                                       'parent_info', 'doc_generate']
        App_name = ['app_docs']


        for item in load_url_pattern_names(fins_url.urlpatterns,
                                           Exeption_app_list_default, Exeption_app_list_no_login,
                                           Exeption_name_list_default, Exeption_name_list_no_login, App_name):
            self.client.login(username='b@b.ru', password='123')

            # tests access permited
            self.dbuser.access_app_docs = True
            self.dbuser.save(using=get_db_name())
            response = self.client.get(
                str(item), HTTP_HOST='mysubdomain.testserver')
            print(str(item) + ' - start access level 2')
            self.assertEqual(response.status_code, 200)
            print(str(item) + ' - checked access level 2')

            # tests access level denied
            self.dbuser.access_app_docs = False
            self.dbuser.save(using=get_db_name())
            response = self.client.get(
                str(item), HTTP_HOST='mysubdomain.testserver', follow=True)
            print(str(item) + ' - start access level 0')
            self.assertRedirects(response,
                                 sub_reverse(settings.ACCESS_DENIED_TO_APP_REDIRECT_URL, subdomain='mysubdomain'),
                                 status_code=302, target_status_code=200,
                                 msg_prefix='', fetch_redirect_response=True)
            print(str(item) + ' - checked access level 0')