from .forms import EXTRA
from django.conf import settings
import os
from app_docs import models as app_docs_models


def formset_file_enrich(formset_file, files):
    i=0
    for f in formset_file:
        if i < (len(formset_file) - EXTRA):
            f.path = os.path.join(settings.MEDIA_URL, str(files[i].file) )
            f.filename = str(files[i].file).split('/', 1)[1]
            i+=1
        else:
            f.path = ''
            f.filename = ''


def filter_extra_data(new_qs):


    i=0
    for doc in new_qs:

        #parent name info
        if doc.parent is not None:
            doc.parent_full = app_docs_models.Agreement.objects.get(pk=doc.parent).name
        else:
            doc.parent_full = None

        # has attachment info
        if len(doc.agreement_files.all()) != 0:
            doc.has_attachment = 'Да'

            # has scan info
            if len(doc.agreement_files.all()) != 0:
                doc.has_scan = 'Нет'
                for file in doc.agreement_files.all():
                    if file.type == 1:
                        doc.has_scan = 'Да'


            # has source file info
            if len(doc.agreement_files.all()) != 0:
                doc.has_src = 'Нет'
                for file in doc.agreement_files.all():
                    if file.type == 2:
                        doc.has_src = 'Да'
        else:
            doc.has_attachment = 'Нет'
            doc.has_scan = 'Нет'
            doc.has_src = 'Нет'

        i+=1

    return new_qs


def child_finder(all, parents_list, flag):
    '''
    creates a list of CHILDREN
    '''
    for p in parents_list:
        for item in all:
            if item.parent == p.id:
                parents_list.append(item)
                all = all.exclude(pk=item.pk)
                flag = True

    if flag is True:
        return child_finder(all, parents_list, False)
    else:
        return parents_list
