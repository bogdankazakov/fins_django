from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from app_auth.decorators import access_company_level
from app_docs.decorators import *
from app_docs.forms import *
from django.urls import reverse_lazy
from django.template.loader import render_to_string
from django.shortcuts import get_object_or_404, redirect, render
from app_setup.tools import log_data
from app_docs import models as app_docs_models
from django.http import JsonResponse
from django.contrib import messages
from fins.global_functions import history_action_verbose, history_record
from app_signup import models as app_signup_models
import logging
from app_docs.tools import *
import os
from django.core.files.storage import default_storage
from fins.global_functions import get_db_name
import pytz
from datetime import datetime
from app_docs.filters import *
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from rest_framework.decorators import api_view
from rest_framework.response import Response
import math
from app_docs.serializers import *
from rest_framework import status

logger = logging.getLogger(__name__)


@login_required
@access_company_level
def index(request, ):
    context = {
        'obj': app_docs_models.Agreement,
    }
    return render(request, 'app_docs/new/index.html', context)


