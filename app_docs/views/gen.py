from app_docs.decorators import *
from app_auth.decorators import access_company_level
from django.contrib.auth.decorators import login_required
from app_project import models as app_project_models
from django.http import HttpResponse
import datetime
# from docxtpl import DocxTemplate
from app_docs.docgen import generate_doc
from app_docs import models as app_docs_models
from django.conf import settings
import os

@login_required
@access_company_level
@access_app_docs_section
def doc_generate(request, pk, type):
    """
    0 - agreement
    1 - accounting_docs
    :param request:
    :param pk:
    :param type:
    :return:
    """
    if type == '0':
        doc = app_docs_models.Agreement.objects.get(pk=pk)
    else:
        doc = app_docs_models.AccountingDocs.objects.get(pk=pk)

    response = HttpResponse() # модель docxtpl глючил в докер - настроить
    # if type == '0' and doc.doc_template:
    #     tmpl_linked_to_doc = doc.doc_template
    #     tmpl_full_path = os.path.join(settings.MEDIA_URL, str(tmpl_linked_to_doc.file) )
    #     tmpl_full_path_adopted=  str(tmpl_full_path).split('/', 1)[1]
    #     template = DocxTemplate(tmpl_full_path_adopted)
    #     context, filename = generate_doc(doc, type)
    #     template.render(context)
    #     content_type = 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
    #     response = HttpResponse(content_type=content_type)
    #     response['Content-Disposition'] = 'attachment; filename={}.docx'.format(filename)
    #
    #     template.get_docx().save(response)
    #
    # else:
    #     response = HttpResponse()
    return response



