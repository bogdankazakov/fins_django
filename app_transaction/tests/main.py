from django.test import TestCase, Client, RequestFactory
from app_signup import models as app_signup_models
from django.contrib.sites.models import Site
from subdomains.utils import reverse as sub_reverse
from django.conf import settings
from app_auth.tests import load_url_pattern_names
from fins import urls  as fins_url
import psycopg2
from fins.threadlocal import thread_local
from app_transaction import views as app_transaction_views
from app_transaction import models as app_transaction_models
from app_project import models as app_project_models
from django.contrib.messages.storage.fallback import FallbackStorage
from app_setup import models as app_setup_models
from django.test import LiveServerTestCase
from selenium.webdriver.support.ui import Select
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import time
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
import os
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from django.conf import settings
from django.contrib.auth import (
    SESSION_KEY, BACKEND_SESSION_KEY, HASH_SESSION_KEY,
    get_user_model
)
from django.contrib.sessions.backends.db import SessionStore
from fins.global_functions import get_db_name_test
from datetime import datetime, date
from decimal import *
from fins.db_setup_test import db_setup, db_setup_converter
from app_transaction.converter import *
from django.db.models import Sum
from django.db.models import Case, When, DateField

def basic_data(project_1, sub_1, puprose_outcome, ac1):
    data = {
                    'project': [str(project_1.pk)],
                    'type': ['False'],
                    'subcontractor': [str(sub_1.pk)],
                    'purpose': [str(puprose_outcome.pk)],
                    'unit': '',
                    'account': [str(ac1.pk)],
                    'date_fact': '',
                    'date_doc': ['2019-03-02'],
                    'sum_before_taxes': ['0,00'],
                    'sum_after_taxes': ['0,00'],
                    'agreement_start_date': ['2019-03-02'],
                    'agreement_end_date': ['2019-03-02'],
                    'pay_model': ['0'],
                    'pay_delay': ['0'],
                    'description': [''],
                    'comment': [''],
                    'estimation_link': [''],
                    'act_date': [''],
                    'status': ['0'],
                    'doc_request': ['0'],
                    'parent': [''],
                }
    return data

class AccessPermissionsTest(TestCase):


    def setUp(self):
        self.client = Client()
        self.factory = RequestFactory()

        site = Site.objects.get(pk=1)
        site.domain = 'testserver'
        site.save()

        c = app_signup_models.Account(name='MyCompany', subdomain='mysubdomain')
        c.save()
        c.create_database()
        c.fill_database()

        user = app_signup_models.User(
            first_name='UserFirst',
            last_name='UserLast',
            email='b@b.ru')
        user.set_password('123')
        user.created_account_pk = c.pk
        user.save()
        user.account.add(c)

        unit1 = app_setup_models.Unit.objects.using(get_db_name_test()).all()[0]
        userdb = app_setup_models.DBUser.objects.using(get_db_name_test()).get(user_id=user.pk)
        userdb.access_app_project = 2
        userdb.unit = unit1
        userdb.save(using=get_db_name_test())


        self.my_user = user
        self.account = c
        self.dbuser = app_setup_models.DBUser.objects.using(get_db_name_test()).get(
            user_id=user.pk)

        #fill db ... stupidly)

        self.client.login(username='b@b.ru', password='123')
        link = sub_reverse('app_dashboard:index',subdomain='mysubdomain')
        request = self.factory.get(link)
        request.user = self.my_user
        request.account = self.account
        db_name_1 = get_db_name_test()

        @thread_local(using_db=db_name_1)
        def tread(request):
            response = app_setup_views.subcontractor_create(request)
            return response

    def tearDown(self):

        #   we delete created account db
        connection_params = {
            'database': settings.MAIN_DB['NAME'],
            'host': settings.MAIN_DB['HOST'],
            'user': settings.MAIN_DB['USER'],
            'password': settings.MAIN_DB['PASSWORD']
        }
        connection = psycopg2.connect(**connection_params)
        connection.set_isolation_level(
            psycopg2.extensions.ISOLATION_LEVEL_AUTOCOMMIT
        )
        cursor = connection.cursor()

        for obj in app_signup_models.Account.objects.all():
            name = 'account_' + str(obj.pk)

            cursor.execute("SELECT pg_terminate_backend(pg_stat_activity.pid)"
                           " FROM pg_stat_activity"
                           " WHERE pg_stat_activity.datname = '%s' AND pid <> pg_backend_pid();" % name)
            cursor.execute('DROP DATABASE IF EXISTS "%s"' % name)
        connection.close()

    def test_access_permission(self):
        """
        test app_setup access and redirect if denied
        Exceptions are given in exception list

        """

        Exeption_app_list_default = ['admin', ]
        Exeption_app_list_no_login = ['app_about', 'app_auth', 'app_signup',]
        Exeption_name_list_default = []
        Exeption_name_list_no_login = [
            'transaction_create',
            'transaction_save',
            'transaction_update',
            'transaction_delete',
            'transaction_create_project',
            'transaction_update_project',
            'transaction_delete_project',
            'transaction_create_project_period',
            'transaction_update_project_period',
            'transaction_delete_project_period',
            'transfer_create',
            'transfer_save',
            'transfer_update',
            'transfer_delete',
            'ajax_load_purpose',
            'ajax_subcontractor_info',
            'ajax_account_info',
            'ajax_mycompany_info',
            'ajax_list_saver',
            'transaction_docs',
            'excel_export',
                                       ]
        App_name = ['app_transaction']


        for item in load_url_pattern_names(fins_url.urlpatterns,
                                           Exeption_app_list_default, Exeption_app_list_no_login,
                                           Exeption_name_list_default, Exeption_name_list_no_login, App_name):
            self.client.login(username='b@b.ru', password='123')

            # tests access permited
            self.dbuser.access_app_project = 2
            self.dbuser.save(using=get_db_name_test())
            response = self.client.get(
                str(item), HTTP_HOST='mysubdomain.testserver')
            print(str(item) + ' - start access level 2')
            self.assertEqual(response.status_code, 200)
            print(str(item) + ' - checked access level 2')

            # tests access level denied
            self.dbuser.access_app_project = 0
            self.dbuser.save(using=get_db_name_test())
            response = self.client.get(
                str(item), HTTP_HOST='mysubdomain.testserver', follow=True)
            print(str(item) + ' - start access level 0')
            self.assertRedirects(response,
                                 sub_reverse(settings.ACCESS_DENIED_TO_APP_REDIRECT_URL, subdomain='mysubdomain'),
                                 status_code=302, target_status_code=200,
                                 msg_prefix='', fetch_redirect_response=True)
            print(str(item) + ' - checked access level 0')

class ConverterTest(TestCase):
        """
        for this test use this table
        https://docs.google.com/spreadsheets/d/1oOGSSI0bipULJoFyI0upMVf_47knnwpqLPbf3uk2R5E/edit#gid=0

        """

        def setUp(self):
            self.client = Client()
            self.factory = RequestFactory()

            site = Site.objects.get(pk=1)
            site.domain = 'testserver'
            site.save()

            c = app_signup_models.Account(name='MyCompany', subdomain='mysubdomain')
            c.save()
            c.create_database()
            c.fill_database()

            user = app_signup_models.User(
                first_name='UserFirst',
                last_name='UserLast',
                email='b@b.ru')
            user.set_password('123')
            user.created_account_pk = c.pk
            user.save()
            user.account.add(c)

            unit1 = app_setup_models.Unit.objects.using(get_db_name_test()).all()[0]
            userdb = app_setup_models.DBUser.objects.using(get_db_name_test()).get(user_id=user.pk)
            userdb.access_app_project = 2
            userdb.unit = unit1
            userdb.save(using=get_db_name_test())

            self.my_user = user
            self.account = c

            db_setup_converter(get_db_name_test())
            self.main_currency = app_transaction_models.Currency.objects.using(get_db_name_test()).get(is_default=True)
            self.all = app_transaction_models.Transaction.objects.using(get_db_name_test()).select_related(
                'account__currency').all().annotate(
            date=Case(
                When(date_fact=None, then='date_doc'),
                default='date_fact',
                output_field=DateField(),
            )).order_by('date')



        def tearDown(self):
            #   we delete created account db
            connection_params = {
                'database': settings.MAIN_DB['NAME'],
                'host': settings.MAIN_DB['HOST'],
                'user': settings.MAIN_DB['USER'],
                'password': settings.MAIN_DB['PASSWORD']
            }
            connection = psycopg2.connect(**connection_params)
            connection.set_isolation_level(
                psycopg2.extensions.ISOLATION_LEVEL_AUTOCOMMIT
            )
            cursor = connection.cursor()

            for obj in app_signup_models.Account.objects.all():
                name = 'account_' + str(obj.pk)

                cursor.execute("SELECT pg_terminate_backend(pg_stat_activity.pid)"
                               " FROM pg_stat_activity"
                               " WHERE pg_stat_activity.datname = '%s' AND pid <> pg_backend_pid();" % name)
                cursor.execute('DROP DATABASE IF EXISTS "%s"' % name)
            connection.close()

        def test_if_main_no_conversion(self):
            """tests that if it's a main currency, there is no conversion"""


            transaction = self.all.filter(account__currency=self.main_currency)[0]
            result = converter(transaction, self.main_currency, self.all, True)
            self.assertEqual(transaction.sum_before_taxes, result)

            not_main_currency = app_transaction_models.Currency.objects.using(get_db_name_test()).exclude(name=self.main_currency.name)[0]
            transaction = self.all.filter(account__currency=not_main_currency)[0]
            result = converter(transaction, self.main_currency, self.all, True)
            self.assertNotEqual(transaction.sum_before_taxes, result)

        def test_rate_finder_income_only(self):

            # transaction- no transfer in the same day, first transfer on the way with source NOT main currency
            transaction = self.all.get(
                sum_before_taxes=Decimal('30.00'),
                date_doc=date(2019, 1, 12),
            )
            rate = rate_finder_income_only(self.all, self.main_currency, transaction, True)
            expect = Decimal(700/200)
            self.assertEqual(expect, rate)

            # transaction- no transfer in the same day, first transfer on the way with source IS main currency
            transaction = self.all.get(
                sum_before_taxes=Decimal('10.00'),
                date_doc=date(2019, 1, 1),
            )
            rate = rate_finder_income_only(self.all, self.main_currency, transaction, True)
            expect = Decimal(500/100)
            self.assertEqual(expect, rate)

            # transaction- no transfer in the same day, NO transfer on the way
            transaction = self.all.get(
                sum_before_taxes=Decimal('30.00'),
                date_doc=date(2018, 12, 30),
            )
            rate = rate_finder_income_only(self.all, self.main_currency, transaction, True)
            expect = Decimal('2.5')
            self.assertEqual(expect, rate)


            # transaction in the same day that transfer (from main currency) - first with greater PK than transfer
            # other with smaller
            transaction = self.all.get(
                sum_before_taxes=Decimal('15.00'),
                date_doc=date(2018, 12, 31),
            )
            rate = rate_finder_income_only(self.all, self.main_currency, transaction, True)
            expect = Decimal(500/100)
            self.assertEqual(expect, rate)

            transaction = self.all.get(
                sum_before_taxes=Decimal('5.00'),
                date_doc=date(2018, 12, 31),
            )
            rate = rate_finder_income_only(self.all, self.main_currency, transaction, True)
            expect = Decimal(500/100)
            self.assertEqual(expect, rate)


            # transaction in the same day that transfer (NOT from main currency)
            transaction = self.all.get(
                sum_before_taxes=Decimal('75.00'),
                date_doc=date(2019, 1, 18),
            )
            rate = rate_finder_income_only(self.all, self.main_currency, transaction, True)
            expect = Decimal(420/140)
            self.assertEqual(expect, rate)


            # transaction is transfer with source = main currency
            transaction = self.all.get(
                sum_before_taxes=Decimal('100.00'),
                date_doc=date(2018, 12, 31),
            )
            rate = rate_finder_income_only(self.all, self.main_currency, transaction, True)
            expect = Decimal(500/100)
            self.assertEqual(expect, rate)

            # transaction with status NOT 3
            transaction = self.all.get(
                sum_before_taxes=Decimal('100.00'),
                date_doc=date(2019, 1, 14),
            )
            rate = rate_finder_income_only(self.all, self.main_currency, transaction, True)
            expect = Decimal('2.5')
            self.assertEqual(expect, rate)

            # transaction is transfer with source = NOT main currency
            transaction = self.all.get(
                sum_before_taxes=Decimal('130.00'),
                date_doc=date(2019, 1, 7),
            )
            rate = rate_finder_income_only(self.all, self.main_currency, transaction, True)
            expect = Decimal(700/200)
            self.assertEqual(expect, rate)

        def test_income_conversion(self):
                """tests the conversion of Income"""

                #if is_transfer from the main currency - we take source transaction.
                # if not from the main currency - use default rate
                transaction = self.all.get(
                    sum_before_taxes=Decimal('200.00'),
                    date_doc=date(2019, 1, 2),
                )
                src_tr = self.all.get(pk=transaction.transfer)
                result = converter(transaction, self.main_currency, self.all, True)
                self.assertEqual(src_tr.sum_before_taxes, result)

                transaction = self.all.get(
                    sum_before_taxes=Decimal('130.00'),
                    date_doc=date(2019, 1, 7),
                )

                result = converter(transaction, self.main_currency, self.all, True)
                default_rate = Decimal('2.50')
                expect = default_rate * transaction.sum_before_taxes
                self.assertEqual(expect, result)

                # if not transfer
                # ====== transaction with status NOT '3'. we expect it to use default rate

                transaction = self.all.get(
                    sum_before_taxes=Decimal('100.00'),
                    date_doc=date(2019, 1, 14),
                )
                result = converter(transaction, self.main_currency, self.all, True)
                default_rate = Decimal('2.50')
                expect = default_rate * transaction.sum_before_taxes
                self.assertEqual(expect, result)

                # ====== transaction with status  '3' with 2 transafers earlier: the closest - EUR/RUB,
                # than USD/RUB. it should use rate from USD/RUB

                transaction = self.all.get(
                    sum_before_taxes=Decimal('30.00'),
                    date_doc=date(2019, 1, 12),
                )
                result = converter(transaction, self.main_currency, self.all, True)
                rate = Decimal(700/200)
                expect = rate * transaction.sum_before_taxes
                self.assertEqual(expect, result)

                # ====== transaction with status  '3' with NO transafers earlier. we expect it to use default rate

                transaction = self.all.get(
                    sum_before_taxes=Decimal('30.00'),
                    date_doc=date(2018, 12, 30),
                )
                result = converter(transaction, self.main_currency, self.all, True)
                default_rate = Decimal('2.50')
                expect = default_rate * transaction.sum_before_taxes
                self.assertEqual(expect, result)

        def test_progressive_sum(self):
            transaction = self.all.get(
                sum_before_taxes=Decimal('40.00'),
                date_doc=date(2019, 1, 17),
            )
            sum_from_up, sum_from_down, all_income_sum, all_outcome_sum, extra_sum_from_up = progressive_sum(transaction, self.all)

            self.assertEqual(Decimal('715.00'), all_income_sum)
            self.assertEqual(Decimal('1435.00'), all_outcome_sum)
            self.assertEqual(transaction.sum_before_taxes, sum_from_up)
            self.assertEqual(Decimal('0.00'), sum_from_down)
            self.assertEqual(Decimal('720.00'), extra_sum_from_up)

        def test_down(self):
            transaction = self.all.get(
                sum_before_taxes=Decimal('960.00'),
                date_doc=date(2019, 1, 6),
            )

            sum_from_up, sum_from_down, all_income_sum, all_outcome_sum, extra_sum_from_up = progressive_sum(transaction, self.all)
            sum_from_down_list = down(transaction, self.all, self.main_currency, sum_from_down, True)
            print('sum_from_up', sum_from_up)
            print('sum_from_down', sum_from_down)
            print('sum_from_down_list', sum_from_down_list)
            expect = [Decimal('122.500'), Decimal('700.000'), Decimal('50.00'), Decimal('175.00')]
            self.assertEqual(expect, sum_from_down_list)

        def test_up(self):
            # progressive sum to this transaction > 0, and we need more money that there is in the system,
            # it means this extra money is converted with default rate
            transaction = self.all.get(
                sum_before_taxes=Decimal('960.00'),
                date_doc=date(2019, 1, 6),
            )
            sum_from_up, sum_from_down, all_income_sum, all_outcome_sum, extra_sum_from_up = progressive_sum(transaction, self.all)
            sum_from_up_list = up(transaction, self.all, self.main_currency, sum_from_up, extra_sum_from_up, True)

            # print('sum_from_down', sum_from_down)
            # print('sum_from_up', sum_from_up)
            # print('extra_sum_from_up', extra_sum_from_up)
            # print('all_income_sum', all_income_sum)
            # print('all_outcome_sum', all_outcome_sum)
            # print('sum_from_up_list', sum_from_up_list)
            expect = [Decimal('455.000'), Decimal('105.000'), Decimal('210.000'), Decimal('75.00'), Decimal('225.00'), Decimal('1080.0000')]
            self.assertEqual(expect, sum_from_up_list)



            # progressive sum to this transaction < 0, it means we need first of all
            # close our debt and then only find money for this transaction
            transaction = self.all.get(
                sum_before_taxes=Decimal('40.00'),
                date_doc=date(2019, 1, 17),
            )
            sum_from_up, sum_from_down, all_income_sum, all_outcome_sum, extra_sum_from_up = progressive_sum(transaction, self.all)
            sum_from_up_list = up(transaction, self.all, self.main_currency, sum_from_up, extra_sum_from_up, True)

            expect = [Decimal(420/140*40)]
            self.assertEqual(expect, sum_from_up_list)



            # progressive sum to this transaction < 0,
            # but there is next transaction to satisfy extra but not transaction
            transaction = self.all.get(
                sum_before_taxes=Decimal('50.00'),
                date_doc=date(2019, 1, 19),
            )
            sum_from_up, sum_from_down, all_income_sum, all_outcome_sum, extra_sum_from_up = progressive_sum(transaction, self.all)
            sum_from_up_list = up(transaction, self.all, self.main_currency, sum_from_up, extra_sum_from_up, True)

            expect = [Decimal(420/140*10), Decimal(420/140*40)]
            self.assertEqual(expect, sum_from_up_list)





            # progressive sum to this transaction < 0
            # but there is next transaction to satisfy extra and transaction
            transaction = self.all.get(
                sum_before_taxes=Decimal('5.00'),
                date_doc=date(2019, 1, 21),
            )
            sum_from_up, sum_from_down, all_income_sum, all_outcome_sum, extra_sum_from_up = progressive_sum(transaction, self.all)
            sum_from_up_list = up(transaction, self.all, self.main_currency, sum_from_up, extra_sum_from_up, True)

            expect = [Decimal(420/140*5)]
            self.assertEqual(expect, sum_from_up_list)





            # progressive sum to this transaction < 0 + not enough money in system
            transaction = self.all.get(
                sum_before_taxes=Decimal('20.00'),
                date_doc=date(2019, 1, 24),
            )
            sum_from_up, sum_from_down, all_income_sum, all_outcome_sum, extra_sum_from_up = progressive_sum(transaction, self.all)
            sum_from_up_list = up(transaction, self.all, self.main_currency, sum_from_up, extra_sum_from_up, True)

            default_rate = Decimal('2.50')
            expect = [transaction.sum_before_taxes * default_rate]
            self.assertEqual(expect, sum_from_up_list)



            # progressive sum to this transaction > 0 + not enough money in system
            transaction = self.all.get(
                sum_before_taxes=Decimal('20.00'),
                date_doc=date(2019, 1, 24),
            )

            tex = self.all.get(
                sum_before_taxes=Decimal('50.00'),
                date_doc=date(2019, 1, 23),
            )

            all = self.all.exclude(pk=tex.pk)
            sum_from_up, sum_from_down, all_income_sum, all_outcome_sum, extra_sum_from_up = progressive_sum(transaction, all)
            sum_from_up_list = up(transaction, all, self.main_currency, sum_from_up, extra_sum_from_up, True)

            default_rate = Decimal('2.50')

            expect = [Decimal('5.00') * 420/140, Decimal('10.00') * default_rate]
            self.assertEqual(expect, sum_from_up_list)

        def test_outcome_conversion(self):
                """tests the conversion of Income"""

                # if is_transfer to the main currency - we take source transaction.
                transaction = self.all.get(
                    sum_before_taxes=Decimal('140.00'),
                    date_doc=date(2019, 1, 16),
                )
                rec_tr = self.all.get(pk=transaction.transfer)
                result = converter(transaction, self.main_currency, self.all, True)
                self.assertEqual(rec_tr.sum_before_taxes, result)

                # if status != 3, use default rate
                transaction = self.all.get(
                    sum_before_taxes=Decimal('200.00'),
                    date_doc=date(2019, 1, 13),
                )
                result = converter(transaction, self.main_currency, self.all, True)
                default_rate = Decimal('2.50')
                expect = default_rate * transaction.sum_before_taxes
                self.assertEqual(expect, result)


                # STAKE MECHANISM

                # Is_transfer NOT to the main currency.
                # +
                # progressive sum to this transaction < 0, it means we need first of all
                # close our debt and then only find money for this transaction
                transaction = self.all.get(
                    sum_before_taxes=Decimal('40.00'),
                    date_doc=date(2019, 1, 17),
                )

                result = converter(transaction, self.main_currency, self.all, True)
                default_rate = Decimal(420/140)
                expect = default_rate * transaction.sum_before_taxes
                self.assertEqual(expect, result)



