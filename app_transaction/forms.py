from django.forms import ModelForm, Form
from app_signup import models as app_signup_models
from app_setup import models as app_setup_models
from app_transaction import models as app_transaction_models
from app_project import models as app_project_models
from django import forms
from django.utils.translation import ugettext_lazy as _
from django.forms import formset_factory, modelformset_factory
from fins.global_functions import BOOL_CHOICES_YESNO


class TransactionForm(ModelForm):


    sub_taxetype = forms.CharField(
        label='Система налогооблажения',
        required=False,
        widget=forms.TextInput(
            attrs={'type': 'text', 'disabled':'true'}
                                )
    )
    sub_legalform = forms.CharField(
        label='ЮП форма',
        required=False,
        widget=forms.TextInput(
            attrs={'type': 'text', 'disabled':'true'}
                                )
    )
    currency = forms.CharField(
        label='Валюта',
        required=False,
        widget=forms.TextInput(
            attrs={'type': 'text', 'disabled':'true'}
                                )
    )
    mycompanytaxe = forms.CharField(
        label='Налог',
        required=False,
        widget=forms.TextInput(
            attrs={'type': 'text', 'disabled':'true'}
                                )
    )
    sum_after_taxes = forms.DecimalField(
        label='Сумма транзакции после налогов',
        required=False,
        widget=forms.TextInput(
            attrs={'type': 'text', 'disabled':'true'}
                                )
    )
    sum_taxes = forms.DecimalField(
        label='Сумма налогов',
        required=False,
        widget=forms.TextInput(
            attrs={'type': 'text', 'disabled':'true'}
                                )
    )

    class Meta:
        model = app_transaction_models.Transaction
        fields = (
            'project',
            'type',
            'subcontractor',
            'sub_taxetype',
            'sub_legalform',
            'purpose',
            'unit',
            'account',
            'currency',
            'mycompanytaxe',
            'date_fact',
            'date_doc',
            'sum_before_taxes',
            'sum_after_taxes',
            'sum_taxes',
            'agreement_start_date',
            'agreement_end_date',
            'pay_model',
            'pay_delay',
            'description',
            'comment',
            'estimation_link',
            'act_date',
            'status',
            'doc_request',
            'hidden',
            'is_editable',
            'parent',
        )

        widgets = {
            'date_fact': forms.TextInput(attrs={'type': 'date'}),
            'date_doc': forms.TextInput(attrs={'type': 'date', 'readonly':'true'}),
            'agreement_start_date': forms.TextInput(attrs={'type': 'date'}),
            'agreement_end_date': forms.TextInput(attrs={'type': 'date'}),
            'act_date': forms.TextInput(attrs={'type': 'date'}),
            # 'hidden': forms.CheckboxInput(),
            'sum_before_taxes': forms.TextInput(attrs={'type': 'text', 'disabled':'true'}),
            'sum_after_taxes': forms.TextInput(attrs={'type': 'text', 'disabled':'true'}),
            'sum_taxes': forms.TextInput(attrs={'type': 'text', 'disabled':'true'}),
            'subcontractor': forms.Select(attrs={'required': 'true'}),
            'account': forms.Select(attrs={'required': 'true'}),
            'purpose': forms.Select(attrs={'required': 'true'}),
            'project': forms.Select(attrs={'required': 'true'}),

        }


    def __init__(self, *args, **kwargs):

        if 'user' in kwargs:
            user = kwargs.pop('user')
        else:
            user = None
        super().__init__(*args, **kwargs)

        # show project that USER have access to
        if user is not None and 'project' in self.fields:
            dbuser = app_setup_models.DBUser.objects.get(user_id=user.id)
            access_level = dbuser.access_app_project
            if access_level == 0:
                all = app_project_models.Project.objects.none()
            elif access_level == 1:
                all = app_project_models.Project.objects.filter(teamprojects_in_project__user=dbuser).order_by('-pk')
            elif access_level == 3:
                all = app_project_models.Project.objects.filter(teamprojects_in_project__user__unit=dbuser.unit).order_by('-pk')
            else:
                all = app_project_models.Project.objects.all().order_by('-pk')
            self.fields['project'].queryset = all
        # hides HIDDEN and IS_EDITABLE forms for user that doesn't have access
        if user is not None:
            dbuser = app_setup_models.DBUser.objects.get(user_id=user.id)
            access_level = dbuser.access_can_block
            if access_level == 0:
                self.fields['hidden'].widget = forms.Select(choices=BOOL_CHOICES_YESNO)
                self.fields['is_editable'].widget = forms.Select(choices=BOOL_CHOICES_YESNO)
            else:
                self.fields['hidden'].widget = forms.Select(attrs={'hidden': 'true'}, choices=BOOL_CHOICES_YESNO)
                self.fields['is_editable'].widget = forms.Select(attrs={'hidden': 'true'}, choices=BOOL_CHOICES_YESNO)


        # dependant list Type - Purpose
        default = app_transaction_models.Transaction._meta.get_field('type').default
        self.fields['purpose'].queryset = app_transaction_models.Purpose.objects.filter(type=default).order_by('name')
        if 'type' in self.data:
            try:
                type = self.data.get('type')
                self.fields['purpose'].queryset = app_transaction_models.Purpose.objects.filter(type=type).order_by('name')
            except (ValueError, TypeError):
                pass  # invalid input from the client; ignore and fallback to empty City queryset
        elif self.instance.pk:
            qs = app_transaction_models.Purpose.objects.filter(type=self.instance.type).order_by('name')
            self.fields['purpose'].queryset = qs

class TransactionProjectForm(TransactionForm):

    class Meta(TransactionForm.Meta):
        exclude = ('project',)



class TransactionAgencyProjectForm(TransactionProjectForm):
    create_income = forms.BooleanField(required=False, help_text='Будет создана зеркальная транзакция дохода. Контрагент будет тот же, что и в расходе. Не забудьте его заменить!')
    class Meta(TransactionProjectForm.Meta):
        fields = TransactionProjectForm.Meta.fields + ('create_income',)



class TransferForm(ModelForm):

    currency = forms.CharField(
        label='Валюта',
        required=False,
        widget=forms.TextInput(
            attrs={'type': 'text', 'disabled':'true'}
                                )
    )

    sub_taxetype = forms.CharField(
        label='Система налогооблажения',
        required=False,
        widget=forms.TextInput(
            attrs={'type': 'text', 'disabled':'true', 'hidden': 'true'}
                                )
    )
    sub_legalform = forms.CharField(
        label='ЮП форма',
        required=False,
        widget=forms.TextInput(
            attrs={'type': 'text', 'disabled':'true', 'hidden': 'true'}
                                )
    )

    sum_after_taxes = forms.DecimalField(
        label='Сумма транзакции после налогов',
        required=False,
        widget=forms.TextInput(
            attrs={'type': 'text', }
                                )
    )
    sum_taxes = forms.DecimalField(
        label='Сумма налогов',
        required=False,
        widget=forms.TextInput(
            attrs={'type': 'text',  'disabled':'true', }
                                )
    )

    class Meta:
        model = app_transaction_models.Transaction
        fields = (
            'account',
            'currency',
            'date_fact',
            'sum_before_taxes',
            'sum_after_taxes',
            'sum_taxes',
            'comment',
            'status',
            'subcontractor',
            'sub_taxetype',
            'sub_legalform',
        )

        widgets = {
            'account': forms.Select(attrs={'required':'true'}),
            'date_fact': forms.TextInput(attrs={'type': 'date', 'required':'true'}),
            'hidden': forms.CheckboxInput(),
            'sum_before_taxes': forms.TextInput(attrs={'type': 'text'}),
            'subcontractor': forms.Select(attrs={'hidden': 'true'}),
        }




# TransferFormSet = formset_factory(TransferForm, min_num=2, max_num=2)
TransferFormSet = modelformset_factory(
    app_transaction_models.Transaction,
    form=TransferForm,

)
class TransferCompanyForm(Form):
    companies = forms.ModelChoiceField(
        queryset=app_transaction_models.Subcontractor.objects.filter(is_my_company=True),
        required=True,
        label='Моя компания',
        initial=0

    )

    hidden = forms.ChoiceField(
        choices=BOOL_CHOICES_YESNO,
        required=True,
        label='Скрыта',
        initial=False

    )

    is_editable = forms.ChoiceField(
        choices=BOOL_CHOICES_YESNO,
        required=True,
        label='Можно менять',
        initial=True

    )

    def __init__(self, *args, **kwargs):

        if 'user' in kwargs:
            user = kwargs.pop('user')
        else:
            user = None
        super().__init__(*args, **kwargs)

        # hides HIDDEN and IS_EDITABLE forms for user that doesn't have access
        if user is not None:
            dbuser = app_setup_models.DBUser.objects.get(user_id=user.id)
            access_level = dbuser.access_can_block
            if access_level == 0:
                self.fields['hidden'].widget = forms.Select(choices=BOOL_CHOICES_YESNO)
                self.fields['is_editable'].widget = forms.Select(choices=BOOL_CHOICES_YESNO)
            else:
                self.fields['hidden'].widget = forms.Select(attrs={'hidden': 'true'}, choices=BOOL_CHOICES_YESNO)
                self.fields['is_editable'].widget = forms.Select(attrs={'hidden': 'true'}, choices=BOOL_CHOICES_YESNO)


TransactionListFormSet = modelformset_factory(app_transaction_models.Transaction,
                                          fields=(
                                              'date_fact',
                                              'status',
                                          )
                                          , extra=0,
                                          widgets={
                                              'date_fact': forms.TextInput(attrs={'type': 'date'}),
                                          }
                                          )