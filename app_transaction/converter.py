from app_transaction import models as app_transaction_models
from decimal import *
from itertools import chain
from django.db.models import Q
from django.db.models import Sum

def default_rate(main_currency, transaction, test):
    if test:
        rate = Decimal('2.5')
    else:
        rates = app_transaction_models.DefaultRate.objects.all()
        for rate in rates:
            if rate.target == main_currency and rate.source == transaction.account.currency:
                rate = rate.rate
                break
            elif rate.target == transaction.account.currency and rate.source == main_currency:
                rate = Decimal(1 / rate.rate)
                break
            else:
                rate = None

    return rate

def rate_finder_income_only(all, main_currency, transaction, test):

    rate = 0

    # if transaction and trasfer are in the same day - we use this trasfer rate.
    # if there were multiple transferes - we take the first
    today_transfers = all.filter(date_doc=transaction.date, is_transfer=True, type = True)
    if len(today_transfers) != 0:
        today_transfer = today_transfers[0]
        source_tr = all.get(pk=today_transfer.transfer)
        source_cur = source_tr.account.currency
        if source_cur == main_currency:
            rate = source_tr.sum_before_taxes / today_transfer.sum_before_taxes
            return rate


    # here we search for early transfer transaction to use it's rate
    index = index_finder(all, transaction)
    while index >= 0:

        tr = all[index]
        if tr.is_transfer:
            source_tr = all.get(pk=tr.transfer)
            source_cur = source_tr.account.currency
            if source_cur == main_currency and tr.account.currency == transaction.account.currency:
                rate = source_tr.sum_before_taxes / tr.sum_before_taxes
                break
        index = index - 1

    # if it's status is PAYED and we found a rate (means we have found transfer) - use it,
    # otherwise use default system rate value
    if transaction.status == 3 and rate != 0:
        rate = rate
    else:
        rate = default_rate(main_currency, transaction, test)
    return rate

def index_finder(list, element):
    """
    :param list: queryset
    :return: index of element in queryset
    """
    idx = 0
    for el in list:
        if el.pk == element.pk:
            break
        idx += 1
    return idx

def progressive_sum(transaction, all):
    # we need to know the sum of all income and outcome transaction before conveted transaction
    # if there is any transactions in the same date than conveting transaction
    # for income we add it, for outcome - we use PK to compare them

    all_income = all.filter(type=True).filter(account__currency=transaction.account.currency)
    all_outcome = all.filter(type=False).filter(account__currency=transaction.account.currency)

    # допсиать чтобы была дата системы
    all_income_sum = all_income.filter(
        date__lte=transaction.date).aggregate(Sum('sum_before_taxes'))[
                         'sum_before_taxes__sum'] or Decimal('0.00')


    q = Q(date__lt=transaction.date)
    same_date_outcome = all_outcome.filter(date=transaction.date)
    if len(same_date_outcome) > 1:  # if there is any transactions in the same date than conveting transaction
        for t in same_date_outcome:
            if t.pk != transaction.pk:  # if it's not converting transaction
                if t.pk < transaction.pk:  # if Pk is smaller not PK converting transaction
                    q = q | Q(pk=t.pk)
    all_outcome_sum = all_outcome.filter(q).aggregate(Sum('sum_before_taxes'))[
                          'sum_before_taxes__sum'] or Decimal('0.00')
    delta = all_income_sum - all_outcome_sum

    if delta >= 0:
        marker = delta - transaction.sum_before_taxes
        if marker < 0:
            sum_from_up = - marker
            sum_from_down = transaction.sum_before_taxes + marker
        else:
            sum_from_up = Decimal('0.00')
            sum_from_down = transaction.sum_before_taxes
        extra_sum_from_up = Decimal('0.00')
    else:
        sum_from_up = transaction.sum_before_taxes
        sum_from_down = Decimal('0.00')
        extra_sum_from_up = -delta

    print(sum_from_up, sum_from_down, all_income_sum, all_outcome_sum, extra_sum_from_up)
    return sum_from_up, sum_from_down, all_income_sum, all_outcome_sum, extra_sum_from_up

def down(transaction, all, main_currency, sum_from_down, test):
    """
    calculate DOWN the list (from last to early transactions).
    take last PAYED income transaction (it means we now the rate) and check if the sum is enough
    for the outcome. if not - "take" the money from this transaction and search for the next
    :return:
    """
    all_cur = all.filter(account__currency=transaction.account.currency).filter(date__lte=transaction.date)
    index = len(all_cur) - 1
    sum_from_down_list = []
    while index >= 0:
        tr = all_cur[index]
        if tr.type == True and tr.status == 3:
            rate = rate_finder_income_only(all, main_currency, tr, test)
            d = tr.sum_before_taxes - sum_from_down
            # print('d', d, tr.name, tr.sum_before_taxes, rate)
            if d < 0:
                number_from_down = round(tr.sum_before_taxes * rate, 2)
                sum_from_down_list.append(number_from_down)
                sum_from_down = -d
            else:
                number_from_down = round(sum_from_down * rate, 2)
                sum_from_down_list.append(number_from_down)
                break
        index = index - 1

    return sum_from_down_list

def up(transaction, all, main_currency,sum_from_up, extra_sum_from_up, test):
    all_cur = all.filter(account__currency=transaction.account.currency).filter(date__gt=transaction.date)
    total_sum_from_up = sum_from_up + extra_sum_from_up
    index = 0
    sum_from_up_list = []
    extra_sum_from_up_list = []
    while index <= len(all_cur) - 1:
        tr = all_cur[index]
        if tr.type == True and tr.status == 3:
            rate = rate_finder_income_only(all, main_currency, tr, test)

            d = tr.sum_before_taxes - total_sum_from_up
            # print('d', d, tr.name, tr.sum_before_taxes, rate)

            if d < 0: # not enough money in the transaction for all need
                if sum(extra_sum_from_up_list) != extra_sum_from_up:

                    d2 = tr.sum_before_taxes - extra_sum_from_up
                    if d2 < 0: # not enough money even for extra need
                        extra_sum_from_up_list.append(tr.sum_before_taxes)
                        extra_sum_from_up = -d2
                    else: # enough money for extra need but no enough money  for sum_from_up need
                        extra_sum_from_up_list.append(extra_sum_from_up)
                        extra_sum_from_up = Decimal('0.00')

                        number_from_up = round(d2 * rate,2)
                        sum_from_up_list.append(number_from_up)
                        sum_from_up = sum_from_up - d2

                    total_sum_from_up = -d

                else:
                    number_from_up = round(tr.sum_before_taxes * rate, 2)
                    sum_from_up_list.append(number_from_up)
                    sum_from_up = -d
                    total_sum_from_up = -d
            else:
                number_from_up = round(sum_from_up * rate, 2)
                sum_from_up_list.append(number_from_up)
                total_sum_from_up = 0
                break
        index = index + 1
        # print('sum_from_up_list', sum_from_up_list)
        # print('sum(extra_sum_from_up_list)', sum(extra_sum_from_up_list))

    if total_sum_from_up != 0:
        rate = default_rate(main_currency, transaction, test)
        if extra_sum_from_up == Decimal('0.00'):
            number_from_up = round(total_sum_from_up * rate, 2)
        else:
            number_from_up = round(sum_from_up * rate, 2)
        sum_from_up_list.append(number_from_up)

    return sum_from_up_list

def converter(transaction, main_currency, all, test):
    """

    :param t: transaction to convert
    :param main_currency: currency convert TO
    :return: converted transaction sum before taxes
    """
    if transaction.account.currency == main_currency:
        # if transaction is in main currency, don't need to convert
        number = transaction.sum_before_taxes
        return number
    else:
        if transaction.type == True:
            if transaction.is_transfer:
                #if it's a transfer from the main currency
                #  - we simply take source transaction sum
                # if not from the main currency - use default rate
                # pay status doesn't matter
                source_tr = all.get(pk=transaction.transfer)
                source_cur = source_tr.account.currency
                if source_cur == main_currency:
                    number = source_tr.sum_before_taxes
                    return number
                else:
                    rate = default_rate(main_currency, transaction, test)
                    number = round(transaction.sum_before_taxes * rate, 2)
                    return number

            # here we search for early transfer transaction to use it's rate
            rate = rate_finder_income_only(all, main_currency, transaction, test)
            number = round(transaction.sum_before_taxes * rate, 2)
            return number
        else:
            if transaction.is_transfer:
                # if it's a transfer to the main currency
                # - we simply take reciever transaction sum
                # if not to the main currency - use stake mechanism
                # pay status does matter if reciever is not main currency
                rec_tr = all.get(pk=transaction.transfer)
                rec_cur = rec_tr.account.currency
                if rec_cur == main_currency:
                    number = rec_tr.sum_before_taxes
                    return number

            if transaction.status != 3:
                rate = default_rate(main_currency, transaction, test)
                number = round(transaction.sum_before_taxes * rate, 2)
                return number



            # here we have a stake method:

            # we need to know the sum of all income and outcome transaction before conveted transaction
            sum_from_up, sum_from_down, a, b, extra_sum_from_up = progressive_sum(transaction, all)

            #calculate DOWN the list (from last to early transactions)
            sum_from_down_list = down(transaction, all, main_currency, sum_from_down, test)

            #calculate UP the list (from early to last transactions)
            sum_from_up_list = up(transaction, all, main_currency, sum_from_up, extra_sum_from_up, test)


            # поправить дату(и сделать тест) и default rate + input для проверки конверсии

            total_sum_list = sum_from_down_list + sum_from_up_list
            number = sum(total_sum_list)
            return number

