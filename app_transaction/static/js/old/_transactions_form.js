$(function () {

    var type_conditions = function () {
        var source_element = $("#id_type");

        // change unit display
        var id_unit = $("#id_unit");
        if (source_element.val() === 'True'){
            id_unit.prop('disabled', true);
        }
        else {
            id_unit.prop('disabled', false);
            }
      };

    var type_conditions2 = function () {

        // dependant dropdown type - purpose
        var url = $("#transactionForm").attr("data-purposes-url");
        var typeId = $("#id_type").val();
          $.ajax({
            url: url,
            data: {
              'type': typeId
            },
            success: function (data) {
              $("#id_purpose").html(data);
            }
          });

      };

    var sbt_conditions = function () {
        var source_element = $("#id_sum_before_taxes");
        var target_element_1 = $("#id_sum_after_taxes");
        var target_element_2 = $("#id_sum_taxes");

        // recalculate sum AFTER taxe if sum BEFORE taxe has changed
        var result_1 = (source_element.val() * taxe).toFixed(2);
        target_element_1.val(result_1);
        var result_2 = (source_element.val() * taxe - source_element.val()).toFixed(2);
        target_element_2.val(result_2);
      };

    var sat_conditions = function () {
        var source_element = $("#id_sum_after_taxes");
        var target_element_1 = $("#id_sum_before_taxes");
        var target_element_2 = $("#id_sum_taxes");

        // recalculate sum BEFORE taxe if sum AFTER taxe has changed
        var result_1 = (source_element.val() / taxe).toFixed(2);
        target_element_1.val(result_1);
        var result_2 = (source_element.val() - source_element.val() / taxe).toFixed(2);
        target_element_2.val(result_2);
      };

    var subcontractor_conditions = function () {
        var source_element = $("#id_subcontractor");
        var pk = source_element.val();

        // load taxetype and legal form of subcontractor selected
        var url = $("#transactionForm").attr("data-subcontractor-url");
          $.ajax({
            url: url,
            data: {
              'pk': pk
            },
            success: function (data) {
              // load info taxetype, taxe and legalform
              $("#id_sub_taxetype").val(data.legalform);
              $("#id_sub_legalform").val(data.taxetype);
              taxe = data.taxevalue;

              // make sum editable
              $("#id_sum_before_taxes").prop('disabled', false);
              $("#id_sum_after_taxes").prop('disabled', false);
              $("#id_sum_taxes").prop('disabled', false);

              // recalculate sum due to new subcontractor taxes
              sbt_conditions();


            }
          });


      };

    var account_conditions = function () {
        var source_element = $("#id_account");
        var pk = source_element.val();

        // load currency of account selected
        var url = $("#transactionForm").attr("data-account-url");
          $.ajax({
            url: url,
            data: {
              'pk': pk
            },
            success: function (data) {
              $("#id_currency").val(data.currency);
            }
          });

      };

    $("#modal-global").on("change", "#id_type", type_conditions);
    $("#modal-global").on("change", "#id_type", type_conditions2);
    $("#modal-global").on("change", "#id_subcontractor", subcontractor_conditions);
    $("#modal-global").on("change", "#id_account", account_conditions);
    $("#modal-global").on("change", "#id_sum_before_taxes", sbt_conditions);
    $("#modal-global").on("change", "#id_sum_after_taxes", sat_conditions);

// export default type_conditions;
// export default subcontractor_conditions;
// export default account_conditions;

});

