from app_auth.decorators import access_company_level
from django.contrib.auth.decorators import login_required
from app_transaction.decorators import *
from django.http import JsonResponse
from app_transaction.forms import *
from django.template.loader import render_to_string
from django.shortcuts import get_object_or_404, render
from django.contrib import messages
import logging
from app_setup.tools import log_data
from app_transaction.tools import *
from app_transaction.filters import *
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.db.models import Case, When, DateField
from app_project import models as app_project_models
from app_project.tools import period_tool, statistics
from app_project.views import wording as app_project_wording
from app_setup import models as app_setup_models

from fins.global_functions import history_record
from rest_framework.decorators import api_view
from rest_framework.response import Response
from app_transaction.serializers import *
from datetime import datetime
import pytz
from rest_framework import status
import math

# ===Logger====

logger = logging.getLogger(__name__)


# ===SETUP====

Form = TransactionForm
Serializer = TransactionSerializer
model = app_transaction_models.Subcontractor
def get_item(pk):
    qs = app_transaction_models.Transaction.objects.get(pk=pk)
    return qs

# ===========





@login_required
@access_company_level
@access_app_transactions_list
def transaction_index(request, get_access_all):
    context = {
        'obj': app_transaction_models.Transaction,
        'form': TransactionForm(user=request.user),
        'formset' : TransferFormSet(),
        'form_company': TransferCompanyForm(),
        'user_can_block': app_setup_models.DBUser.objects.get(user_id=request.user.id).access_can_block,
    }


    return render(request, 'app_transaction/new/index.html', context)



@api_view(['GET', 'POST'])
@login_required
@access_company_level
@access_app_transactions_list
def transaction_list(request, get_access_all):
    """
    List all items or create a new items.
    """
    if request.method == 'GET':
        offset =  int(request.GET.get('offset'))
        page =  int(request.GET.get('page'))
        filter_param =  request.GET.get('filter')
        qs = get_access_all()
        all = filterMain(qs, filter_param)

        start = (page - 1) * offset
        end =  page * offset
        items = all[start:end]

        total_pages = math.ceil(all.count() / offset)

        serializer = Serializer(items, many=True)

        data = {
            'total_pages': total_pages,
            'list': serializer.data,
        }

        return Response(data)

    elif request.method == 'POST':
        print(request.data)
        serializer = Serializer(data=request.data)

        if serializer.is_valid():

            instance = serializer.save(
                name='EMPTY',
                created_by=app_setup_models.DBUser.objects.get(user_id=request.user.pk),
                modified_at=pytz.utc.localize(datetime.utcnow()),
                modified_by=app_setup_models.DBUser.objects.get(user_id=request.user.pk)
            )
            numgen(instance)
            instance.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET', 'PUT', 'DELETE'])
def transaction_detail(request, pk):
    """
    Retrieve, update or delete a items.
    """
    try:
        item = get_item(pk)
    except model.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = Serializer(item)

        accounts = app_transaction_models.Account.objects.all()
        purposes = app_transaction_models.Purpose.objects.all()
        subcontractors = app_transaction_models.Subcontractor.objects.all()
        extra = {
            'accounts': AccountSerializer(accounts, many=True).data,
            'purposes': PurposeSerializer(purposes, many=True).data,
            'subcontractors': SubcontractorSerializer(subcontractors, many=True).data
        }


        deletable, related_obj = item.can_delete()
        new_serializer = {'deletable': deletable}
        new_serializer.update(serializer.data)
        new_serializer.update(extra)
        return Response(new_serializer)

    elif request.method == 'PUT':
        serializer = Serializer(item, data=request.data)
        if serializer.is_valid(raise_exception=True):
            serializer.save(
                modified_at=pytz.utc.localize(datetime.utcnow()),
                modified_by=app_setup_models.DBUser.objects.get(user_id=request.user.pk)
            )
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        item.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

@api_view(['GET', 'PUT', 'DELETE'])
def transfere_detail(request, pk):
    """
    Retrieve, update or delete a items.
    """
    try:
        item = get_item(pk)
    except model.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = Serializer(item)

        accounts = app_transaction_models.Account.objects.all()
        purposes = app_transaction_models.Purpose.objects.all()
        subcontractors = app_transaction_models.Subcontractor.objects.all()
        extra = {
            'accounts': AccountSerializer(accounts, many=True).data,
            'purposes': PurposeSerializer(purposes, many=True).data,
            'subcontractors': SubcontractorSerializer(subcontractors, many=True).data
        }


        deletable, related_obj = item.can_delete()
        new_serializer = {'deletable': deletable}
        new_serializer.update(serializer.data)
        new_serializer.update(extra)
        return Response(new_serializer)

    elif request.method == 'PUT':
        serializer = Serializer(item, data=request.data)
        if serializer.is_valid(raise_exception=True):
            serializer.save(
                modified_at=pytz.utc.localize(datetime.utcnow()),
                modified_by=app_setup_models.DBUser.objects.get(user_id=request.user.pk)
            )
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        item.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


@api_view(['GET', ])
def api_transaction_history(request):
    """
    Retrieve,
    """
    if request.method == 'GET':
        history = app_transaction_models.Transaction.history.all()[:3]
        data = {
            'records': history_record(history, 'Транзакция', True),
        }

        return Response(data)




@api_view(['GET', ])
@login_required
@access_company_level
@access_app_transactions_list
def api_transaction_extra(request, get_access_all):

    if request.method == 'GET':

        t_status = []
        for stat in app_transaction_models.Transaction.STATUS_CHOICES:
            t_status.append({
                'id': stat[0],
                'name': stat[1],
            })

        pay_model = []
        for model in app_transaction_models.Transaction.PAY_MODEL_CHOICES:
            pay_model.append({
                'id': model[0],
                'name': model[1],
            })

        project_stage = []
        for st in app_project_models.Project.STAGES_CHOICES:
            project_stage.append({
                'id': st[0],
                'name': st[1],
            })

        doc_request = []
        for el in app_transaction_models.Transaction.REQUEST_CHOICES:
            doc_request.append({
                'id': el[0],
                'name': el[1],
            })

        accounts = app_transaction_models.Account.objects.all()
        purposes = app_transaction_models.Purpose.objects.all()
        subcontractors = app_transaction_models.Subcontractor.objects.all()
        subcontractor_types = app_transaction_models.SubcontractorType.objects.all()
        sub_taxe_type = app_transaction_models.TaxeType.objects.all()

        projects = app_project_models.Project.objects.all()
        projects_cat = app_project_models.ProjectCategory.objects.all()
        projects_subcat = app_project_models.ProjectSubcategory.objects.all()
        brands = app_project_models.Brand.objects.all()
        units = app_setup_models.Unit.objects.all()
        users = app_setup_models.DBUser.objects.all()


        extra = {
            'accounts': AccountSerializer(accounts, many=True).data,
            'purposes': PurposeSerializer(purposes, many=True).data,
            'subcontractors': SubcontractorSerializer(subcontractors, many=True).data,
            'subcontractor_type': SubcontractorTypeSerializer(subcontractor_types, many=True).data,
            'transaction_status': t_status,
            'sub_taxe_type': TaxetypeSerializer(sub_taxe_type, many=True).data,
            'pay_model': pay_model,
            'projects': ProjectSerializer(projects, many=True).data,
            'projects_cat': ProjectCategotySerializer(projects_cat, many=True).data,
            'projects_subcat': ProjectSubcategorySerializer(projects_subcat, many=True).data,
            'brands': BrandSerializer(brands, many=True).data,
            'project_stage': project_stage,
            'units': UnitSerializer(units, many=True).data,
            'users': UserSerializer(users, many=True).data,
            'doc_request': doc_request,
        }



        data = {
            'extra': extra,
        }

        return Response(data)

