from django.apps import AppConfig


class AppSetupConfig(AppConfig):
    name = 'app_setup'
