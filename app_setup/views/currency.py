from app_project import models as app_project_models
from app_transaction import models as app_transaction_models
from app_auth.decorators import access_company_level
from django.contrib.auth.decorators import login_required
from app_setup.decorators import *
from app_setup.forms import *
from django.shortcuts import get_object_or_404, redirect, render
import logging
from fins.global_functions import history_action_verbose, history_record
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from app_setup.serializers import DefaultRateSerializer
from datetime import datetime
import pytz
import math
from rest_framework import serializers
from app_setup.tools import currency_rate_builder,currency_rate_killer, delete_checker
from django.db.models import Case, When, DateField
from app_transaction.converter import converter

# ===Logger====

logger = logging.getLogger(__name__)


# ===SETUP====

class CurrencySerializer(serializers.ModelSerializer):
    class Meta:
        model = app_transaction_models.Currency
        fields = '__all__'

Serializer = CurrencySerializer

model = app_transaction_models.Currency
def get_items():
    qs = app_transaction_models.Currency.objects.all()
    return qs
def get_item(pk):
    qs = app_transaction_models.Currency.objects.get(pk=pk)
    return qs
def get_history():
    qs = app_transaction_models.Currency.history.all()
    return qs

def get_all_rates():
    qs = app_transaction_models.DefaultRate.objects.all()
    return qs

instance_name = 'Валюта'
sex = False
# ===========


@api_view(['GET', 'POST'])
@login_required
@access_company_level
@access_app_setup
def list(request):
    """
    List all items or create a new items.
    """
    if request.method == 'GET':


        offset = int(request.GET.get('offset'))
        page = int(request.GET.get('page'))
        searchquery = request.GET.get('searchquery')

        all = get_items()
        all = all.filter(name__icontains=searchquery)

        start = (page - 1) * offset
        end = page * offset
        items = all[start:end]
        total_pages = math.ceil(all.count() / offset)

        rates = get_all_rates()


        data = {
            'total_pages': total_pages,
            'list': Serializer(items, many=True).data,
            'rates': DefaultRateSerializer(rates, many=True).data,
        }

        return Response(data)

    elif request.method == 'POST':
        serializer = Serializer(data=request.data)
        if serializer.is_valid():
            instance = serializer.save(
                created_by=app_setup_models.DBUser.objects.get(user_id=request.user.pk),
                modified_at=pytz.utc.localize(datetime.utcnow()),
                modified_by=app_setup_models.DBUser.objects.get(user_id=request.user.pk)
            )
            currency_rate_builder(instance)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

@api_view(['GET', 'PUT', 'DELETE'])
@login_required
@access_company_level
@access_app_setup
def detail(request, pk):
    """
    Retrieve, update or delete.
    """
    try:
        item = get_item(pk)
    except model.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = Serializer(item)

        deletable, related_obj = delete_checker(item)
        new_serializer = {'deletable': deletable}
        new_serializer.update(serializer.data)
        return Response(new_serializer)

    elif request.method == 'PUT':
        serializer = Serializer(item, data=request.data)
        if serializer.is_valid(raise_exception=True):
            serializer.save(
                modified_at=pytz.utc.localize(datetime.utcnow()),
                modified_by=app_setup_models.DBUser.objects.get(user_id=request.user.pk)
            )
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        currency_rate_killer(item)
        item.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


@api_view(['GET', ])
@login_required
@access_company_level
@access_app_setup
def history(request):
    """
    Retrieve
    """
    if request.method == 'GET':
        history = get_history()[:35]
        records = history_record(history, instance_name, sex)
        data = {'records': records}
        return Response(data)





@api_view(['GET', 'PUT', 'DELETE'])
@login_required
@access_company_level
@access_app_setup
def rate(request, pk):
    """
    Retrieve, update or delete.
    """
    try:
        item = app_transaction_models.DefaultRate.objects.get(pk=pk)
    except app_transaction_models.DefaultRate.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)


    if request.method == 'GET':
        serializer = DefaultRateSerializer(item)
        deletable, related_obj = item.can_delete()
        new_serializer = {'deletable': deletable}
        new_serializer.update(serializer.data)
        return Response(new_serializer)

    elif request.method == 'PUT':
        serializer = DefaultRateSerializer(item, data=request.data)
        if serializer.is_valid(raise_exception=True):
            serializer.save(
                modified_at=pytz.utc.localize(datetime.utcnow()),
                modified_by=app_setup_models.DBUser.objects.get(user_id=request.user.pk)
            )
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)



@api_view(['GET', 'POST', ])
@login_required
@access_company_level
@access_app_setup
def default(request):

    if request.method == 'GET':
        all = app_transaction_models.Currency.objects.all()
        data = {
            'list': Serializer(all, many=True).data,
        }
        return Response(data)

    elif request.method == 'POST':
        default = request.data['id']

        current = app_transaction_models.Currency.objects.get(is_default=True)
        current.is_default = False
        current.save()

        new = app_transaction_models.Currency.objects.get(pk=default)
        new.is_default = True
        new.save()


        return Response(status=status.HTTP_200_OK)
@api_view(['GET', ])
@login_required
@access_company_level
@access_app_setup
def convert(request):

    if request.method == 'GET':
        main_currency_id = request.GET['currency']
        transaction = request.GET['transaction']

        all = app_transaction_models.Transaction.objects.all().annotate(
            date=Case(
                When(date_fact=None, then='date_doc'),
                default='date_fact',
                output_field=DateField(),
            )).order_by('date')

        transaction = all.get(name=transaction)
        main_currency = app_transaction_models.Currency.objects.get(pk=main_currency_id)
        result = converter(transaction, main_currency, all, False)
        message = result
        # try:
        #     transaction = all.get(name=transaction)
        #     result = converter(transaction, main_currency, all, False)
        #     message = str(result) + '(' + str(main_currency.name) + '). Исходная валюта транзакции ('+ str(transaction.account.currency) + ')'
        # except Exception:
        #     message = 'Такой транзакции не обнаружено'

        return Response(message)





#
#
# @login_required
# @access_company_level
# @access_app_setup
# def currency_list(request):
#     default_currency_form = CurrencyDefaultForm(using=get_db_name(request))
#     conversion_form = ConverionForm()
#     context = {
#         'list': get_all(),
#         'rates': get_all_rates(),
#         'wording': wording,
#         'templates': templates,
#         'js': js,
#         'url': url,
#         'default_currency_form': default_currency_form,
#         'conversion_form': conversion_form,
#     }
#     return render(request, templates['list'], context)
#
#
#
#
# @login_required
# @access_company_level
# @access_app_setup
# def currency_create(request):
#     if request.method == 'POST':
#         form = Form(request.POST)
#     else:
#         form = Form()
#     return currency_save(request, form, templates['create'], True)
#
#
#
# @login_required
# @access_company_level
# @access_app_setup
# def currency_save(request, form, template_name, creation):
#     data = dict()
#     if request.method == 'POST':
#         if form.is_valid():
#             instance = form.save(commit=False)
#             log_data(instance, request)
#             instance.save()
#             if creation:
#                 currency_rate_builder(instance)
#             data['form_is_valid'] = True
#             context = {
#                 'list': get_all(),
#                 'rates': get_all_rates(),
#                 'wording': wording,
#                 'js': js,
#                 'url': url,
#             }
#             data['html_list'] = render_to_string(templates['partial_list'], context)
#             data['html_list_rate'] = render_to_string(templates['partial_list2'], context)
#         else:
#             messages.error(request, wording['validation_error'])
#
#     if request.method == 'GET':
#         data['form_is_valid'] = False
#     context = {
#                 'form': form,
#                 'wording': wording,
#                 'js': js,
#                 'url': url,
#                }
#     data['html_form'] = render_to_string(template_name, context, request=request)
#     return JsonResponse(data)
#
#
# @login_required
# @access_company_level
# @access_app_setup
# def currency_update(request, pk):
#     item = get_item(pk)
#     if request.method == 'POST':
#         form = Form(request.POST, instance=item)
#     else:
#         form = Form(instance=item)
#     return currency_save(request, form, templates['update'], False)
#
# @login_required
# @access_company_level
# @access_app_setup
# def currency_delete(request, pk):
#     data = dict()
#     item = get_item(pk)
#     if request.method == 'POST':
#         currency_rate_killer(item)
#         item.delete()
#
#         data['form_is_valid'] = True
#         context = {
#             'list': get_all(),
#             'rates': get_all_rates(),
#             'wording': wording,
#             'js': js,
#             'url': url,
#         }
#         data['html_list'] = render_to_string(templates['partial_list'], context)
#         data['html_list_rate'] = render_to_string(templates['partial_list2'], context)
#
#     if request.method == 'GET':
#         deletable, related_obj = delete_checker(item)
#         context = {
#             'item': item,
#             'deletable':deletable,
#             'wording': wording,
#             'js': js,
#             'url': url,
#         }
#         data['html_form'] = render_to_string(templates['delete'],context,request=request,)
#     return JsonResponse(data)
#
#
#
#
# @login_required
# @access_company_level
# @access_app_setup
# def currency_history(request):
#
#     history = app_transaction_models.Currency.history.all()[:3]
#
#     context = {
#         'records': history_record(history, 'Валюта', True),
#         'wording':wording,
#     }
#
#     data = {}
#     data['html_form'] = render_to_string(templates['history'], context, request=request)
#
#     return JsonResponse(data)
#
#
# @login_required
# @access_company_level
# @access_app_setup
# def currency_default(request):
#     data = {}
#     pk = request.GET.get('value')
#     current = app_transaction_models.Currency.objects.get(is_default=True)
#     current.is_default = False
#     current.save()
#
#     new = app_transaction_models.Currency.objects.get(pk=pk)
#     new.is_default = True
#     new.save()
#
#
#     return JsonResponse(data)
#
#
#
#
# @login_required
# @access_company_level
# @access_app_setup
# def currency_conversion(request):
#     form = ConverionForm(request.POST)
#     data = {}
#     if request.method == 'POST':
#         if form.is_valid():
#             transaction = form.cleaned_data['transaction']
#             main_currency = form.cleaned_data['currency']
#             all = app_transaction_models.Transaction.objects.all().annotate(
#                 date=Case(
#                     When(date_fact=None, then='date_doc'),
#                     default='date_fact',
#                     output_field=DateField(),
#                 )).order_by('date')
#             try:
#                 transaction = all.get(name=transaction)
#                 result = converter(transaction, main_currency, all, False)
#                 message = str(result) + '(' + str(main_currency.name) + '). Исходная валюта транзакции ('+ str(transaction.account.currency) + ')'
#             except Exception:
#                 message = 'Такой транзакции не обнаружено'
#             context = {
#                 'message': message
#             }
#             data['html'] = render_to_string('app_setup/currencies/partial_conversion.html', context)
#
#
#         else:
#             messages.error(request, wording['validation_error'])
#
#     return JsonResponse(data)
#
#
#
#
#
#
#
# @login_required
# @access_company_level
# @access_app_setup
# def rate_update(request, pk):
#     item = app_transaction_models.DefaultRate.objects.get(pk=pk)
#     if request.method == 'POST':
#         form = RateForm(request.POST, instance=item)
#     else:
#         form = RateForm(instance=item)
#     return rate_save(request, form, templates['update2'])
#
#
#
# @login_required
# @access_company_level
# @access_app_setup
# def rate_save(request, form, template_name):
#     data = dict()
#     if request.method == 'POST':
#         if form.is_valid():
#             instance = form.save(commit=False)
#             log_data(instance, request)
#             instance.save()
#             data['form_is_valid'] = True
#             context = {
#                 'list': get_all(),
#                 'rates': get_all_rates(),
#                 'wording': wording,
#                 'js': js,
#                 'url': url,
#             }
#             data['html_list'] = render_to_string(templates['partial_list2'], context)
#         else:
#             messages.error(request, wording['validation_error'])
#
#     if request.method == 'GET':
#         data['form_is_valid'] = False
#     context = {
#                 'form': form,
#                 'wording': wording,
#                 'js': js,
#                 'url': url,
#                }
#     data['html_form'] = render_to_string(template_name, context, request=request)
#     return JsonResponse(data)
#
#
#
#
#
#
#
#
# from app_transaction import models as app_transaction_models
# from app_auth.decorators import access_company_level
# from django.contrib.auth.decorators import login_required
# from app_setup.decorators import *
# from django.http import JsonResponse
# from app_setup.forms import *
# from django.template.loader import render_to_string
# from django.shortcuts import get_object_or_404, redirect, render
# from django.contrib import messages
# import logging
# from app_setup.tools import *
# from django.urls import reverse_lazy
# from fins.global_functions import history_action_verbose, history_record, get_db_name
# from app_transaction.converter import converter
# from django.db.models import Case, When, DateField
#
# logger = logging.getLogger(__name__)
#
# # ===SETUP====
#
# Form = CurrencyForm
# wording = {
#     'btn_create': 'Создать',
#     'btn_add': 'Добавить валюту ',
#     'btn_delete': 'Удалить',
#     'btn_cancel': 'Отмена',
#     'btn_update': 'Редактировать',
#     'btn_save': 'Сохранить',
#     'btn_history': 'История',
#     'h1_list': 'Список валют',
#     'h1_create': 'Создать валюту',
#     'h1_update': 'Редактировать валюту',
#     'h1_update2': 'Редактировать курс',
#     'h1_delete': 'Удалить валюту',
#     'h1_history': 'История изменений валюты',
#     'validation_error': 'Ошибка валидации',
#     'not_deletable': 'Мы не можем удалить элемент - он используется в системе. Сначала удалите его из всех транзакций.',
#     'approve_delete': ' Вы уверены, что хотите удалить валюту ',
# }
# templates = {
#     'list': 'app_setup/currencies/currencies.html',
#     'partial_list': 'app_setup/currencies/partial_list.html',
#     'partial_list2': 'app_setup/currencies/partial_list_rate.html',
#     'create': 'app_setup/currencies/partial_create.html',
#     'update': 'app_setup/currencies/partial_update.html',
#     'update2': 'app_setup/currencies/partial_update_rate.html',
#     'delete': 'app_setup/currencies/partial_delete.html',
#     'history': 'app_setup/currencies/partial_history.html',
# }
#
# js = {
#     'jscreate': 'js-create-currency',
#     'jshistory': 'js-history-currency',
#
# }
# url = {
#     'create': reverse_lazy('app_setup:currency_create'),
#     'history': reverse_lazy('app_setup:currency_history'),
# }
# def get_all():
#     qs = app_transaction_models.Currency.objects.all()
#     return qs
#
# def get_item(pk):
#     item = get_object_or_404(app_transaction_models.Currency, pk=pk)
#     return item
#
# def get_all_rates():
#     qs = app_transaction_models.DefaultRate.objects.all()
#     return qs
#
#
# # ===========
#
#
# @login_required
# @access_company_level
# @access_app_setup
# def currency_list(request):
#     default_currency_form = CurrencyDefaultForm(using=get_db_name(request))
#     conversion_form = ConverionForm()
#     context = {
#         'list': get_all(),
#         'rates': get_all_rates(),
#         'wording': wording,
#         'templates': templates,
#         'js': js,
#         'url': url,
#         'default_currency_form': default_currency_form,
#         'conversion_form': conversion_form,
#     }
#     return render(request, templates['list'], context)
#
#
#
#
# @login_required
# @access_company_level
# @access_app_setup
# def currency_create(request):
#     if request.method == 'POST':
#         form = Form(request.POST)
#     else:
#         form = Form()
#     return currency_save(request, form, templates['create'], True)
#
#
#
# @login_required
# @access_company_level
# @access_app_setup
# def currency_save(request, form, template_name, creation):
#     data = dict()
#     if request.method == 'POST':
#         if form.is_valid():
#             instance = form.save(commit=False)
#             log_data(instance, request)
#             instance.save()
#             if creation:
#                 currency_rate_builder(instance)
#             data['form_is_valid'] = True
#             context = {
#                 'list': get_all(),
#                 'rates': get_all_rates(),
#                 'wording': wording,
#                 'js': js,
#                 'url': url,
#             }
#             data['html_list'] = render_to_string(templates['partial_list'], context)
#             data['html_list_rate'] = render_to_string(templates['partial_list2'], context)
#         else:
#             messages.error(request, wording['validation_error'])
#
#     if request.method == 'GET':
#         data['form_is_valid'] = False
#     context = {
#                 'form': form,
#                 'wording': wording,
#                 'js': js,
#                 'url': url,
#                }
#     data['html_form'] = render_to_string(template_name, context, request=request)
#     return JsonResponse(data)
#
#
# @login_required
# @access_company_level
# @access_app_setup
# def currency_update(request, pk):
#     item = get_item(pk)
#     if request.method == 'POST':
#         form = Form(request.POST, instance=item)
#     else:
#         form = Form(instance=item)
#     return currency_save(request, form, templates['update'], False)
#
# @login_required
# @access_company_level
# @access_app_setup
# def currency_delete(request, pk):
#     data = dict()
#     item = get_item(pk)
#     if request.method == 'POST':
#         currency_rate_killer(item)
#         item.delete()
#
#         data['form_is_valid'] = True
#         context = {
#             'list': get_all(),
#             'rates': get_all_rates(),
#             'wording': wording,
#             'js': js,
#             'url': url,
#         }
#         data['html_list'] = render_to_string(templates['partial_list'], context)
#         data['html_list_rate'] = render_to_string(templates['partial_list2'], context)
#
#     if request.method == 'GET':
#         deletable, related_obj = delete_checker(item)
#         context = {
#             'item': item,
#             'deletable':deletable,
#             'wording': wording,
#             'js': js,
#             'url': url,
#         }
#         data['html_form'] = render_to_string(templates['delete'],context,request=request,)
#     return JsonResponse(data)
#
#
#
#
# @login_required
# @access_company_level
# @access_app_setup
# def currency_history(request):
#
#     history = app_transaction_models.Currency.history.all()[:3]
#
#     context = {
#         'records': history_record(history, 'Валюта', True),
#         'wording':wording,
#     }
#
#     data = {}
#     data['html_form'] = render_to_string(templates['history'], context, request=request)
#
#     return JsonResponse(data)
#
#
# @login_required
# @access_company_level
# @access_app_setup
# def currency_default(request):
#     data = {}
#     pk = request.GET.get('value')
#     current = app_transaction_models.Currency.objects.get(is_default=True)
#     current.is_default = False
#     current.save()
#
#     new = app_transaction_models.Currency.objects.get(pk=pk)
#     new.is_default = True
#     new.save()
#
#
#     return JsonResponse(data)
#
#
#
#
# @login_required
# @access_company_level
# @access_app_setup
# def currency_conversion(request):
#     form = ConverionForm(request.POST)
#     data = {}
#     if request.method == 'POST':
#         if form.is_valid():
#             transaction = form.cleaned_data['transaction']
#             main_currency = form.cleaned_data['currency']
#             all = app_transaction_models.Transaction.objects.all().annotate(
#                 date=Case(
#                     When(date_fact=None, then='date_doc'),
#                     default='date_fact',
#                     output_field=DateField(),
#                 )).order_by('date')
#             try:
#                 transaction = all.get(name=transaction)
#                 result = converter(transaction, main_currency, all, False)
#                 message = str(result) + '(' + str(main_currency.name) + '). Исходная валюта транзакции ('+ str(transaction.account.currency) + ')'
#             except Exception:
#                 message = 'Такой транзакции не обнаружено'
#             context = {
#                 'message': message
#             }
#             data['html'] = render_to_string('app_setup/currencies/partial_conversion.html', context)
#
#
#         else:
#             messages.error(request, wording['validation_error'])
#
#     return JsonResponse(data)
#
#
#
#
#
#
#
# @login_required
# @access_company_level
# @access_app_setup
# def rate_update(request, pk):
#     item = app_transaction_models.DefaultRate.objects.get(pk=pk)
#     if request.method == 'POST':
#         form = RateForm(request.POST, instance=item)
#     else:
#         form = RateForm(instance=item)
#     return rate_save(request, form, templates['update2'])
#
#
#
# @login_required
# @access_company_level
# @access_app_setup
# def rate_save(request, form, template_name):
#     data = dict()
#     if request.method == 'POST':
#         if form.is_valid():
#             instance = form.save(commit=False)
#             log_data(instance, request)
#             instance.save()
#             data['form_is_valid'] = True
#             context = {
#                 'list': get_all(),
#                 'rates': get_all_rates(),
#                 'wording': wording,
#                 'js': js,
#                 'url': url,
#             }
#             data['html_list'] = render_to_string(templates['partial_list2'], context)
#         else:
#             messages.error(request, wording['validation_error'])
#
#     if request.method == 'GET':
#         data['form_is_valid'] = False
#     context = {
#                 'form': form,
#                 'wording': wording,
#                 'js': js,
#                 'url': url,
#                }
#     data['html_form'] = render_to_string(template_name, context, request=request)
#     return JsonResponse(data)
#
#
#
#

