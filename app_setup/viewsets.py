from rest_framework import viewsets
from app_transaction import models as app_transaction_models
from .serializers import SubcontractorSerializer

class SubcontractorViewSet(viewsets.ModelViewSet):
    queryset = app_transaction_models.Subcontractor.objects.all()
    serializer_class = SubcontractorSerializer