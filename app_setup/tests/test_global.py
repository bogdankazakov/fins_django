from django.test import TestCase, Client, RequestFactory
from app_signup import models as app_signup_models
from django.contrib.sites.models import Site
from subdomains.utils import reverse as sub_reverse
from django.conf import settings
from app_auth.tests import load_url_pattern_names
from fins import urls  as fins_url
import psycopg2
from fins.threadlocal import thread_local
from app_setup import views as app_setup_views
from app_transaction import models as app_transaction_models
from django.contrib.messages.storage.fallback import FallbackStorage
from app_setup import models as app_setup_models
from fins.global_functions import get_db_name_test



class GlobalTest(TestCase):


    def setUp(self):
        self.client = Client()
        self.factory = RequestFactory()

        site = Site.objects.get(pk=1)
        site.domain = 'testserver'
        site.save()

        c = app_signup_models.Account(name='MyCompany', subdomain='mysubdomain')
        c.save()
        c.create_database()
        c.fill_database()

        user = app_signup_models.User(
            first_name='UserFirst',
            last_name='UserLast',
            email='b@b.ru')
        user.set_password('123')
        user.created_account_pk = c.pk
        user.save()
        user.account.add(c)

        unit1 = app_setup_models.Unit.objects.using(get_db_name_test()).all()[0]
        userdb = app_setup_models.DBUser.objects.using(get_db_name_test()).get(user_id=user.pk)
        userdb.access_app_setup = 0
        userdb.access_users = 0
        userdb.is_owner = True
        userdb.unit = unit1
        userdb.save(using=get_db_name_test())

        self.my_user = user
        self.account = c
        self.dbuser = app_setup_models.DBUser.objects.using(get_db_name_test()).get(
            user_id=user.pk)

        #fill db ... stupidly)

        self.client.login(username='b@b.ru', password='123')
        link = sub_reverse('app_dashboard:index',subdomain='mysubdomain')
        request = self.factory.get(link)
        request.user = self.my_user
        request.account = self.account
        db_name_1 = get_db_name_test()

        @thread_local(using_db=db_name_1)
        def tread(request):
            response = app_setup_views.subcontractor_create(request)
            return response

    def tearDown(self):

        #   we delete created account db
        connection_params = {
            'database': settings.MAIN_DB['NAME'],
            'host': settings.MAIN_DB['HOST'],
            'user': settings.MAIN_DB['USER'],
            'password': settings.MAIN_DB['PASSWORD']
        }
        connection = psycopg2.connect(**connection_params)
        connection.set_isolation_level(
            psycopg2.extensions.ISOLATION_LEVEL_AUTOCOMMIT
        )
        cursor = connection.cursor()

        for obj in app_signup_models.Account.objects.all():
            name = 'account_' + str(obj.pk)

            cursor.execute("SELECT pg_terminate_backend(pg_stat_activity.pid)"
                           " FROM pg_stat_activity"
                           " WHERE pg_stat_activity.datname = '%s' AND pid <> pg_backend_pid();" % name)
            cursor.execute('DROP DATABASE IF EXISTS "%s"' % name)
        connection.close()

    def test_access_permission(self):
        """
        test app_setup access and redirect if denied
        Exceptions are given in exception list

        """

        Exeption_app_list_default = ['admin', ]
        Exeption_app_list_no_login = ['app_about', 'app_auth', 'app_signup',]
        Exeption_name_list_default = []
        Exeption_name_list_no_login = [
             'user_create', 'user_save', 'user_update', 'user_delete', 'join',
            'subcontractor_create', 'subcontractor_save', 'subcontractor_update', 'subcontractor_delete',
            'subcontractortype_create', 'subcontractortype_save', 'subcontractortype_update', 'subcontractortype_delete',
            'account_create', 'account_save', 'account_update', 'account_delete',
            'currency_create', 'currency_save', 'currency_update', 'currency_delete', 'currency_default',
            'unit_create', 'unit_save', 'unit_update', 'unit_delete',
            'account_create', 'account_save', 'account_update', 'account_delete',
            'purpose_create', 'purpose_save', 'purpose_update', 'purpose_delete',
            'brand_create', 'brand_save', 'brand_update', 'brand_delete',
            'legalform_create', 'legalform_save', 'legalform_update', 'legalform_delete',
            'taxetype_create', 'taxetype_save', 'taxetype_update', 'taxetype_delete',
            'nbsource_create', 'nbsource_save', 'nbsource_update', 'nbsource_delete',
            'nbformat_create', 'nbformat_save', 'nbformat_update', 'nbformat_delete',
            'productcategory_create', 'productcategory_save', 'productcategory_update', 'productcategory_delete',
            'projectproduct_create', 'projectproduct_save', 'projectproduct_update', 'projectproduct_delete',
            'projectcat_create', 'projectcat_save', 'projectcat_update', 'projectcat_delete',
            'projectsubcat_create', 'projectsubcat_save', 'projectsubcat_update', 'projectsubcat_delete',
            'rate_save', 'rate_update',
            'profile', 'profile_update',
            'doc_template_create', 'doc_template_save', 'doc_template_update', 'doc_template_delete',

        ]
        App_name = ['app_setup']


        for item in load_url_pattern_names(fins_url.urlpatterns,
                                           Exeption_app_list_default, Exeption_app_list_no_login,
                                           Exeption_name_list_default, Exeption_name_list_no_login, App_name):
            self.client.login(username='b@b.ru', password='123')

            # tests access permited
            self.dbuser.access_app_setup = 0
            self.dbuser.access_users = 0
            self.dbuser.save(using=get_db_name_test())
            response = self.client.get(
                str(item), HTTP_HOST='mysubdomain.testserver')
            print(str(item) + ' - start access level Permited')
            self.assertEqual(response.status_code, 200)
            print(str(item) + ' - checked access Permited')

            # tests access level denied
            self.dbuser.access_app_setup = 1
            self.dbuser.access_users = 1
            self.dbuser.save(using=get_db_name_test())
            response = self.client.get(
                str(item), HTTP_HOST='mysubdomain.testserver', follow=True)
            print(str(item) + ' - start access Denied')
            self.assertRedirects(response,
                                 sub_reverse(settings.ACCESS_DENIED_TO_APP_REDIRECT_URL, subdomain='mysubdomain'),
                                 status_code=302, target_status_code=200,
                                 msg_prefix='', fetch_redirect_response=True)
            print(str(item) + ' - checked access Denied')

    def test_db_router(self):
        """
        test if DB router works for write and read

        """


        #create as user_1

        self.client.login(username='b@b.ru', password='123')
        link = sub_reverse('app_setup:subcontractor_create',subdomain='mysubdomain')
        data = {
                 'name': 'TestSub_from_user_1',
                 'type': 1,
                 'taxe_type': 1,
                 'legal_form': 1,
                }
        request = self.factory.post(link, data)
        request.user = self.my_user
        request.account = self.account
        db_name_1 = get_db_name_test()

        #need this to make messages work
        setattr(request, 'session', 'session')
        messages = FallbackStorage(request)
        setattr(request, '_messages', messages)


        @thread_local(using_db=db_name_1)
        def tread(request):
            response = app_setup_views.subcontractor_create(request)
            return response


        response = tread(request)
        print(response)


        #create as user_2

        c1 = app_signup_models.Account(name='MyCompany2', subdomain='mysubdomain2')
        c1.save()
        c1.create_database()
        c1.fill_database()

        user2 = app_signup_models.User(
            first_name='UserFirst2',
            last_name='UserLast2',
            email='k@k.ru')
        user2.set_password('123')
        user2.created_account_pk = c1.pk
        user2.save()
        user2.account.add(c1)

        unit1 = app_setup_models.Unit.objects.using(get_db_name_test()).all()[0]
        userdb = app_setup_models.DBUser.objects.using(get_db_name_test()).get(user_id=user2.pk)
        userdb.access_app_setup = 0
        userdb.access_users = 0
        userdb.is_owner = True
        userdb.unit = unit1
        userdb.save(using=get_db_name_test())

        my_user_2 = user2
        account_2 = c1

        self.client.login(username='k@k.ru', password='123')
        link = sub_reverse('app_setup:subcontractor_create',subdomain='mysubdomain')
        data = {
                 'name': ['TestSub_from_user_2'],
                 'type': [1],
                 'taxe_type': [1],
                 'legal_form': [1],
                }
        request = self.factory.post(link, data)
        request.user = my_user_2
        request.account = account_2
        db_name_2 = get_db_name_test()

        #need this to make messages work
        setattr(request, 'session', 'session')
        messages = FallbackStorage(request)
        setattr(request, '_messages', messages)

        @thread_local(using_db=db_name_2)
        def tread(request):
            response = app_setup_views.subcontractor_create(request)
            return response
        response = tread(request)


        self.assertTrue(app_transaction_models.Subcontractor.objects.using(db_name_1).get(name = 'TestSub_from_user_1'))
        self.assertTrue(app_transaction_models.Subcontractor.objects.using(db_name_2).get(name = 'TestSub_from_user_2'))
        try:
            app_transaction_models.Subcontractor.objects.using(db_name_1).get(name = 'TestSub_from_user_2')
            self.assertTrue(1,2)
        except Exception:
            self.assertTrue(1, 1)

        try:
            app_transaction_models.Subcontractor.objects.using(db_name_2).get(name = 'TestSub_from_user_1')
            self.assertTrue(1,2)
        except Exception:
            self.assertTrue(1, 1)

    def test_createdby_modifiedby_log(self):

        '''
        tests that created by is not overwriten by modifyed by
        '''

        # create second user
        user2 = app_signup_models.User(
            first_name='UserFirst2',
            last_name='UserLast2',
            email='b2@b2.ru')
        user2.set_password('123')
        user2.created_account_pk = self.account.pk
        user2.save()
        user2.account.add(self.account)

        unit1 = app_setup_models.Unit.objects.using(get_db_name_test()).all()[0]
        userdb = app_setup_models.DBUser.objects.using(get_db_name_test()).get(user_id=user2.pk)
        userdb.access_app_setup = 0
        userdb.access_users = 0
        userdb.is_owner = True
        userdb.unit = unit1
        userdb.save(using=get_db_name_test())

        # create as first user
        self.client.login(username='b@b.ru', password='123')
        link = sub_reverse('app_setup:subcontractor_create', subdomain='mysubdomain')
        data = {
            'name': ['TestSub'],
            'type': [1],
            'taxe_type': [1],
            'legal_form': [1],
        }
        request = self.factory.post(link, data)
        request.user = self.my_user
        request.account = self.account
        @thread_local(using_db=get_db_name_test())
        def tread(request):
            response = app_setup_views.subcontractor_create(request)
            return response
        response = tread(request)
        new_sub = app_transaction_models.Subcontractor.objects.using(get_db_name_test()).get(name='TestSub')
        creator = app_transaction_models.DBUser.objects.using(get_db_name_test()).get(user_id=self.my_user.id)
        self.assertEqual(new_sub.created_by, creator)
        self.assertEqual(new_sub.modified_by, creator)



        # update as second user
        self.client.login(username='b2@b2.ru', password='123')
        kwargs = {'pk': new_sub.pk}
        link = sub_reverse('app_setup:subcontractor_update', kwargs=kwargs, subdomain='mysubdomain')
        data = {
            'name': ['TestSub2'],
            'type': [1],
            'taxe_type': [1],
            'legal_form': [1],
        }
        request = self.factory.post(link, data)
        request.user = user2
        request.account = self.account
        @thread_local(using_db=get_db_name_test())
        def tread(request):
            response = app_setup_views.subcontractor_update(request, **kwargs)
            return response
        response = tread(request)
        new_sub = app_transaction_models.Subcontractor.objects.using(get_db_name_test()).get(name='TestSub2')
        modifier = app_transaction_models.DBUser.objects.using(get_db_name_test()).get(user_id=user2.id)
        self.assertEqual(new_sub.created_by, creator)
        self.assertEqual(new_sub.modified_by, modifier)


