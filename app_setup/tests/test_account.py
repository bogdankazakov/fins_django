from django.test import TestCase, Client, RequestFactory
from app_signup import models as app_signup_models
from app_transaction import models as app_transaction_models
from app_setup import views as app_setup_views
from django.contrib.sites.models import Site
from subdomains.utils import reverse as sub_reverse
from django.conf import settings
from fins.threadlocal import thread_local
from django.contrib.messages.storage.fallback import FallbackStorage
import psycopg2
from .test_global import get_db_name_test
from app_setup import models as app_setup_models
from app_setup.tests.test_tools import Setup

class AccountsTest(Setup):


    def test_accounts_CRUD(self):

        self.client.login(username='b@b.ru', password='123')

        #create
        link = sub_reverse('app_setup:account_create',subdomain='mysubdomain')
        data = {
                 'name': ['TestAccount'],
                 'type': [0],
                 'currency': [int(app_transaction_models.Currency.objects.using(get_db_name_test()).all()[0].pk)],
                 'company_owner': [int(app_transaction_models.Subcontractor.objects.using(get_db_name_test()).all()[0].pk)],
                }
        request = self.factory.post(link, data)
        request.user = self.my_user
        request.account = self.account
        @thread_local(using_db=get_db_name_test())
        def tread(request):
            response = app_setup_views.account_create(request)
            return response
        response = tread(request)

        created = app_transaction_models.Account.objects.using(get_db_name_test()).get(name = 'TestAccount')
        self.assertEqual(response.status_code, 200)
        self.assertTrue(created)

        #update
        link = sub_reverse('app_setup:account_update', kwargs={'pk': created.pk} ,subdomain='mysubdomain')
        data = {
                 'name': ['TestAccount2'],
                 'type': [0],
                 'currency': [int(app_transaction_models.Currency.objects.using(get_db_name_test()).all()[0].pk)],
                 'company_owner': [int(app_transaction_models.Subcontractor.objects.using(get_db_name_test()).all()[0].pk)],
                }
        request = self.factory.post(link, data)
        request.user = self.my_user
        request.account = self.account
        @thread_local(using_db=get_db_name_test())
        def tread(request):
            response = app_setup_views.account_update(request, created.pk)
            return response

        response = tread(request)

        update = app_transaction_models.Account.objects.using(get_db_name_test()).get(name='TestAccount2')
        self.assertEqual(response.status_code, 200)
        self.assertTrue(update)


        #delete
        link = sub_reverse('app_setup:account_delete',kwargs={'pk': created.pk}, subdomain='mysubdomain')
        data = {}
        request = self.factory.post(link, data)
        request.user = self.my_user
        request.account = self.account
        @thread_local(using_db=get_db_name_test())
        def tread(request):
            response = app_setup_views.account_delete(request, created.pk)
            return response
        response = tread(request)

        deleted = app_transaction_models.Account.objects.using(get_db_name_test()).filter(name = 'TestAccount2')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(deleted), 0)

