from django.test import TestCase, Client, RequestFactory
from app_signup import models as app_signup_models
from app_transaction import models as app_transaction_models
from app_setup import views as app_setup_views
from django.contrib.sites.models import Site
from subdomains.utils import reverse as sub_reverse
from django.conf import settings
from fins.threadlocal import thread_local
from django.contrib.messages.storage.fallback import FallbackStorage
import psycopg2
from .test_global import get_db_name_test
from app_setup.tests.test_tools import Setup



class PurposesTest(Setup):


    def test_purposes_CRUD(self):

        self.client.login(username='b@b.ru', password='123')

        #create
        link = sub_reverse('app_setup:purpose_create',subdomain='mysubdomain')
        data = {
                 'name': ['purpose'],
                 'type': ['True'],
                }
        request = self.factory.post(link, data)
        request.user = self.my_user
        request.account = self.account
        @thread_local(using_db=get_db_name_test())
        def tread(request):
            response = app_setup_views.purpose_create(request)
            return response
        response = tread(request)

        created = app_transaction_models.Purpose.objects.using(get_db_name_test()).get(name = 'purpose')
        self.assertEqual(response.status_code, 200)
        self.assertTrue(created)

        #update
        link = sub_reverse('app_setup:purpose_update', kwargs={'pk': created.pk} ,subdomain='mysubdomain')
        data = {
                 'name': ['purpose2'],
                 'type': ['True'],
                }
        request = self.factory.post(link, data)
        request.user = self.my_user
        request.account = self.account
        @thread_local(using_db=get_db_name_test())
        def tread(request):
            response = app_setup_views.purpose_update(request, created.pk)
            return response

        response = tread(request)

        update = app_transaction_models.Purpose.objects.using(get_db_name_test()).get(name='purpose2')
        self.assertEqual(response.status_code, 200)
        self.assertTrue(update)


        #delete
        link = sub_reverse('app_setup:purpose_delete',kwargs={'pk': created.pk}, subdomain='mysubdomain')
        data = {}
        request = self.factory.post(link, data)
        request.user = self.my_user
        request.account = self.account
        @thread_local(using_db=get_db_name_test())
        def tread(request):
            response = app_setup_views.purpose_delete(request, created.pk)
            return response
        response = tread(request)

        deleted = app_transaction_models.Purpose.objects.using(get_db_name_test()).filter(name = 'purpose2')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(deleted), 0)

