from .test_global import *
from .test_subcontractors import *
from .test_users import *
from .test_account import *
from .test_brand import *
from .test_currency import *
from .test_legalform import *
from .test_nbformat import *
from .test_nbsource import *
from .test_productcategory import *
from .test_projectproduct import *
from .test_projectcat import *
from .test_projectsubcat import *
from .test_purpose import *
from .test_subcontractortype import *
from .test_taxetype import *
from .test_unit import *
from .test_doc_template import *