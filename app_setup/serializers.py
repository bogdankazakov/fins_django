from rest_framework import serializers
from app_transaction import models as app_transaction_models
from app_project import models as app_project_models
from app_setup import models as app_setup_models
from app_signup import models as app_signup_models

def field_creation(model, exclude_fields, extra_fields):
    all_fields = [f.name for f in model._meta.get_fields()]
    result = [item for item in all_fields if item not in exclude_fields] + extra_fields
    return result


class SubcontractorSerializer(serializers.ModelSerializer):
    type_name = serializers.ReadOnlyField(source='type.name', read_only=True)
    taxe_type_name = serializers.ReadOnlyField(source='taxe_type.name', read_only=True)
    legal_form_name = serializers.ReadOnlyField(source='legal_form.name', read_only=True)

    class Meta:
        model = app_transaction_models.Subcontractor
        fields = field_creation(
            app_transaction_models.Subcontractor,
            [
                'subs_accounts',
                'transaction',
                'subs_agreement',
                'mycompany_agreement',
                'mycompany_accounting_doc',
                'subs_accounting_doc',
                'modified_at',
                'modified_by',
                'created_at',
                'created_by',
            ],
            [
                'type_name',
                'taxe_type_name',
                'legal_form_name',
             ] )
        # fields = '__all__'
        # depth = 1

class AccountSerializer(serializers.ModelSerializer):
    type_display = serializers.ReadOnlyField(source='get_type_display', read_only=True)
    currency_display = serializers.ReadOnlyField(source='currency.name', read_only=True)
    company_owner_display = serializers.ReadOnlyField(source='company_owner.name', read_only=True)

    class Meta:
        model = app_transaction_models.Account
        fields = field_creation(
            app_transaction_models.Account,
            [
                'transaction'
            ],
            [
                'type_display',
                'currency_display',
                'company_owner_display',

            ])

class ProjectSubcategorySerializer(serializers.ModelSerializer):
    category_display = serializers.ReadOnlyField(source='category.name', read_only=True)

    class Meta:
        model = app_project_models.ProjectSubcategory
        fields = field_creation(
            app_project_models.ProjectSubcategory,
            [
                'subcategory_projects',
            ],
            [
                'category_display',

            ])

class ProjectProductSerializer(serializers.ModelSerializer):
    type_display = serializers.ReadOnlyField(source='type.name', read_only=True)

    class Meta:
        model = app_project_models.ProjectProduct
        fields = field_creation(
            app_project_models.ProjectProduct,
            [
                'project'
            ],
            [
                'type_display',

            ])



class DefaultRateSerializer(serializers.ModelSerializer):
    source_display = serializers.ReadOnlyField(source='source.name', read_only=True)
    target_display = serializers.ReadOnlyField(source='target.name', read_only=True)

    class Meta:
        model = app_transaction_models.DefaultRate
        fields = field_creation(
            app_transaction_models.DefaultRate,
            [
            ],
            [
                'source_display',
                'target_display',

            ])


class DBUserSerializer(serializers.ModelSerializer):
    access_app_dashboard_display = serializers.ReadOnlyField(source='get_access_app_dashboard_display', read_only=True)
    access_app_docs_display = serializers.ReadOnlyField(source='get_access_app_docs_display', read_only=True)
    access_app_setup_display = serializers.ReadOnlyField(source='get_access_app_setup_display', read_only=True)
    access_app_project_display = serializers.ReadOnlyField(source='get_access_app_project_display', read_only=True)
    access_app_reports_display = serializers.ReadOnlyField(source='get_access_app_reports_display', read_only=True)
    access_users_display = serializers.ReadOnlyField(source='get_access_users_display', read_only=True)
    access_can_block_display = serializers.ReadOnlyField(source='get_access_can_block_display', read_only=True)

    class Meta:
        model = app_setup_models.DBUser
        fields = (
                'id',
                'email',
                'first_name',
                'last_name',
                'invited_by_full',
                'unit',
                'access_app_dashboard_display',
                'access_app_docs_display',
                'access_app_setup_display',
                'access_app_project_display',
                'access_app_reports_display',
                'access_users_display',
                'access_can_block_display',
                'access_app_dashboard',
                'access_app_docs',
                'access_app_setup',
                'access_app_project',
                'access_app_reports',
                'access_users',
                'access_can_block',
                'joined_at',
        )

class UserSerializer(serializers.ModelSerializer):
    localization_display = serializers.ReadOnlyField(source='get_localization_display', read_only=True)
    class Meta:
        model = app_signup_models.User
        fields = (
                'email',
                'first_name',
                'last_name',
                'localization_display',
        )
