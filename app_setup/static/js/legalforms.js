$(function () {

  var loadForm = function () {
    var btn = $(this);
    $.ajax({
      url: btn.attr("data-url"),
      type: 'get',
      dataType: 'json',
      beforeSend: function () {
        $("#modal-global").modal("show");
      },
      success: function (data) {
        $("#modal-global .modal-content").html(data.html_form);
      }
    });
  };

  var saveForm = function () {
    var form = $(this);
    $.ajax({
      url: form.attr("action"),
      data: form.serialize(),
      type: form.attr("method"),
      dataType: 'json',
      success: function (data) {
        if (data.form_is_valid) {
          $("#legalforms_list tbody").html(data.html_list);
          $("#modal-global").modal("hide");
        }
        else {
          $("#modal-global .modal-content").html(data.html_form);
        }
      },
      error: function () {
          console.log("error")
        }
    });
    return false;
  };


  // Create subcontractor
  $(".js-create-legalform").click(loadForm);
  $("#modal-global").on("submit", ".js-legalform-create-form", saveForm);

  // Update subcontractor
  $("#legalforms_list").on("click", ".js-update-legalform", loadForm);
  $("#modal-global").on("submit", ".js-legalform-update-form", saveForm);

  // Delete subcontractor
  $("#legalforms_list").on("click", ".js-delete-legalform", loadForm);
  $("#modal-global").on("submit", ".js-legalform-delete-form", saveForm);


  var loadList = function () {
    var btn = $(this);
    $.ajax({
      url: btn.attr("data-url"),
      type: 'get',
      dataType: 'json',
      beforeSend: function () {
        $("#modal-global").modal("show");
      },
      success: function (data) {
        $("#modal-global .modal-content").html(data.html_form);
      }
    });
  };

    // Show history
  $(".js-history-legalform").click(loadList);

});