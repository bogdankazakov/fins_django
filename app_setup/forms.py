from django.forms import ModelForm, Form
from app_signup import models as app_signup_models
from app_setup import models as app_setup_models
from app_transaction import models as app_transaction_models
from app_project import models as app_project_models
from app_docs import models as app_docs_models
from django import forms


class UserForm(ModelForm):

    class Meta:
        model = app_signup_models.User
        fields = (
            'first_name',
            'last_name',
            'email',
        )

class AccessForm(ModelForm):

    class Meta:
        model = app_setup_models.DBUser
        fields = (
            'access_app_dashboard',
            'access_app_docs',
            'access_app_setup',
            'access_app_project',
            # 'access_app_reports',
            'access_users',
            'access_can_block',
            'unit',
        )


class SubcontractorForm(ModelForm):

    class Meta:
        model = app_transaction_models.Subcontractor
        fields = (
            'name',
            'type',
            'taxe_type',
            'legal_form',
            'site',
            'comment',
            'pay_delay',
            'is_my_company',
            'requisites_full_name',
            'requisites_official_address',
            'requisites_real_address',
            'requisites_inn',
            'requisites_kpp',
            'requisites_okpo',
            'requisites_okdev',
            'requisites_ogrn',
            'requisites_bank_name',
            'requisites_bank_bik',
            'requisites_bank_address',
            'requisites_bank_pay_acc',
            'requisites_bank_cor_acc',
            'requisites_bank_card_number',
            'director_firstname',
            'director_secondname',
            'director_fathername',
            'director_tel',
            'director_email',
            'contact_name',
            'contact_tel',
            'contact_email',
            'contact_messenger_name',
            'contact_messenger_id',
            'contact_social_net',
            'contact_social_link',
        )
        widgets = {
            'is_my_company': forms.CheckboxInput(),

        }

class UnitForm(ModelForm):

    class Meta:
        model = app_transaction_models.Unit
        fields = (
            'name',
        )

class PurposeForm(ModelForm):

    class Meta:
        model = app_transaction_models.Purpose
        fields = (
            'name',
            'type',
        )

class CurrencyForm(ModelForm):

    class Meta:
        model = app_transaction_models.Currency
        fields = (
            'name',
        )

class CurrencyDefaultForm(Form):
    default = forms.ModelChoiceField(
        queryset=app_transaction_models.Currency.objects.all(),
        required=True,
        label='Валюта по умолчанию',
    )

    def __init__(self, *args, **kwargs):
        using = kwargs.pop('using')
        selected = app_transaction_models.Currency.objects.using(using).get(is_default=True)
        super(CurrencyDefaultForm, self).__init__(*args, **kwargs)
        self.fields['default'].initial = selected.pk


class AccountForm(ModelForm):

    class Meta:
        model = app_transaction_models.Account
        fields = (
            'name',
            'type',
            'currency',
            'company_owner',
        )

    def __init__(self, *args, **kwargs):
        # prevent adding all sub to company owner - onle My compnay subs

        using = kwargs.pop('using')
        qs = app_transaction_models.Subcontractor.objects.using(using).filter(is_my_company=True)

        CHOICES = [('', '-----------'),]
        for el in qs:
            choice = (el.id, el.name)
            CHOICES.append(choice)
        CHOICES = tuple(CHOICES)

        super(AccountForm, self).__init__(*args, **kwargs)
        self.fields['company_owner'].widget = forms.Select(choices=CHOICES)

class LegalformForm(ModelForm):

    class Meta:
        model = app_transaction_models.Legalform
        fields = (
            'name',
            'name_full',
        )

class TaxetypeForm(ModelForm):

    class Meta:
        model = app_transaction_models.TaxeType
        fields = (
            'name',
            'multiplicator',
        )

class SubcontractortypeForm(ModelForm):

    class Meta:
        model = app_transaction_models.SubcontractorType
        fields = (
            'name',
        )

class NBsourceForm(ModelForm):

    class Meta:
        model = app_project_models.NBSource
        fields = (
            'name',
        )

class NBformatForm(ModelForm):

    class Meta:
        model = app_project_models.NBFormat
        fields = (
            'name',
        )

class ProductcategoryForm(ModelForm):

    class Meta:
        model = app_project_models.ProductCategory
        fields = (
            'name',
        )

class ProjectproductForm(ModelForm):

    class Meta:
        model = app_project_models.ProjectProduct
        fields = (
            'name',
            'type',
        )

class ProjectcatForm(ModelForm):

    class Meta:
        model = app_project_models.ProjectCategory
        fields = (
            'name',
        )

class ProjectsubcatForm(ModelForm):

    class Meta:
        model = app_project_models.ProjectSubcategory
        fields = (
            'name',
            'category',
        )



class BrandForm(ModelForm):

    class Meta:
        model = app_project_models.Brand
        fields = (
            'name',
        )


class ConverionForm(Form):
    transaction = forms.CharField(
        label='Транзакция для конвертации',
        required=True,
        widget=forms.TextInput(
            attrs={'type': 'text'}
                                ))
    currency = forms.ModelChoiceField(
        queryset=app_transaction_models.Currency.objects.all(),
        required=True,
        label='В какаую валюту перевести',
    )


class RateForm(ModelForm):

    class Meta:
        model = app_transaction_models.DefaultRate
        fields = (
            'rate',
        )

class DocTemplateForm(ModelForm):

    class Meta:
        model = app_docs_models.DocTemplateFile
        fields = (
            'name',
            'file',
        )
        widgets = {
            'file': forms.FileInput,

        }

    def clean(self):
        form_data = self.cleaned_data
        if str(form_data['file']).lower().endswith(('.docx')) is not True:
            self._errors["file"] = ["Файл не в формате DOCX"]
            del form_data['file']

        return form_data