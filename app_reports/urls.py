from django.contrib import admin
from django.urls import path, include, re_path
from django.contrib.auth import views as auth_views
from . import views

app_name = 'app_reports'
urlpatterns = [
    # path('', views.index, name='index'),
    # path('cashflow/', views.cashflow, name='cashflow'),
    # path('pl/', views.pl, name='pl'),
    # path('subcontractors/', views.subcontractors, name='subcontractors'),
    # path('brands/', views.brands, name='brands'),

    path('api/pl/', views.pl),
    path('api/cashflow/', views.cashflow),
    path('api/subcontractors/', views.subcontractors),
    path('api/brands/', views.brands),

]


