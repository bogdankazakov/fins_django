from __future__ import unicode_literals
from django.db import models
from django.core.mail import send_mail
from django.contrib.auth.models import PermissionsMixin
from django.contrib.auth.base_user import AbstractBaseUser
from django.utils.translation import ugettext_lazy as _
from .managers import UserManager
from fins.global_functions import IsDeletableMixin, BOOL_CHOICES_YESNO
from django.conf import settings
import psycopg2
from django.core.management import call_command
from django.db.models.signals import post_save, pre_delete
from django.dispatch import receiver
import os
import shutil



class Account(models.Model, IsDeletableMixin):
    name = models.CharField(_('Название компании'), max_length=500)
    subdomain = models.CharField(_('Субдомен'), max_length=50, unique=True)

    created_at = models.DateTimeField(_('Когда создан'), auto_now_add = True)
    modified_at = models.DateTimeField(_('Когдаf изменен'), auto_now = True)
    is_deleted = models.BooleanField(_('Удалена'),default=False, choices = BOOL_CHOICES_YESNO)
    is_active = models.BooleanField(_('Активен'),default=True, choices = BOOL_CHOICES_YESNO)
    active_state_date = models.DateTimeField(_('Когда изменен статус Активен'), null=True, blank=True)
    is_free = models.BooleanField(_('Бесплатный'),default=False, choices = BOOL_CHOICES_YESNO)
    is_mine = models.BooleanField(_('Мой'),default=False, choices = BOOL_CHOICES_YESNO)


    class Meta:
        ordering = ['name']

    def __str__(self):
        return self.name

    def get_database_name(self):
        return 'account_%d' % self.id

    def _get_internal_db_connection(self):

        connection_params = {
            'database': settings.ACCOUNTS_DB['NAME'],
            'host': settings.ACCOUNTS_DB['HOST'],
            'user': settings.ACCOUNTS_DB['USER'],
            'password': settings.ACCOUNTS_DB['PASSWORD'],
            'port': settings.ACCOUNTS_DB['PORT']
        }

        connection = psycopg2.connect(**connection_params)
        connection.set_isolation_level(
            psycopg2.extensions.ISOLATION_LEVEL_AUTOCOMMIT
        )
        return connection

    def migrate_database(self):
        settings.DATABASES.load_db(self.get_database_name())
        call_command('migrate', database=self.get_database_name(),
                     interactive=False, verbosity=0)

    def create_database(self):

        connection = self._get_internal_db_connection()
        cursor = connection.cursor()


        cursor.execute(
            'CREATE DATABASE "%s" OWNER "%s"' % (
                self.get_database_name(), settings.ACCOUNTS_DB['USER']
            )
        )
        self.migrate_database()

    def drop_database(self):
        connection = self._get_internal_db_connection()
        cursor = connection.cursor()
        cursor.execute(
            'DROP DATABASE IF EXISTS "%s"' % self.get_database_name()
        )


    def fill_database(self):
        from .db_setup import initial_setup
        name = self.get_database_name()
        initial_setup(name)

    def delete_files(self):
        path = os.path.join(settings.MEDIA_ROOT, self.get_database_name())
        shutil.rmtree(path, ignore_errors=True)


@receiver(pre_delete, sender=Account)
def delete_account(sender, instance, **kwargs):
    instance.drop_database()
    instance.delete_files()


class User(AbstractBaseUser, PermissionsMixin, IsDeletableMixin):

    LANG_CHOICES = (
        (0, 'ru'),
        (1, 'en'),
    )
    account = models.ManyToManyField('Account', related_name='users')
    email = models.EmailField(_('Email'), max_length=250, unique=True)
    first_name = models.CharField(_('Имя'), max_length=30)
    last_name = models.CharField(_('Фамилия'), max_length=30)

    created_at = models.DateTimeField(_('Когда создан'), auto_now_add = True)
    is_deleted = models.BooleanField(_('Удален'),default=False, choices = BOOL_CHOICES_YESNO)
    is_staff = models.BooleanField(default=True, choices = BOOL_CHOICES_YESNO)
    is_active = models.BooleanField(default=True, choices = BOOL_CHOICES_YESNO)

    localization = models.PositiveSmallIntegerField(_('Язык'), choices=LANG_CHOICES, default=0)

    objects = UserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    class Meta:
        verbose_name = _('Пользователь')
        verbose_name_plural = _('Пользователи')


    def get_full_name(self):
        '''
        Returns the first_name plus the last_name, with a space in between.
        '''
        full_name = '%s %s' % (self.first_name, self.last_name)
        return full_name.strip()

    def get_short_name(self):
        '''
        Returns the short name for the user.
        '''
        return self.first_name

    def email_user(self, subject, message, from_email=None, **kwargs):
        '''
        Sends an email to this User.
        '''
        send_mail(subject, message, from_email, [self.email], **kwargs)




class Owner(models.Model, IsDeletableMixin):
    user = models.ForeignKey(User, verbose_name='Пользователь', related_name='is_owner', on_delete=models.CASCADE)
    account = models.ForeignKey(Account, verbose_name='Аккаунт', related_name='owners', on_delete=models.CASCADE)

    class Meta:
        ordering = ['account']

    def __str__(self):
        return 'Владельцы аккаунта ' + str(self.account)



class Billing(models.Model, IsDeletableMixin):
    name = models.CharField(_('Номер счета'), max_length=50)
    date_provided = models.DateTimeField(_('Дата выставления счета'), auto_now_add = True)
    period_start = models.DateField(_('Начало тарификации'))
    period_end = models.DateField(_('Конец тарификации'))
    sum_bt = models.DecimalField(_('Сумма до налогов'),max_digits=15, decimal_places=2)
    sum_at = models.DecimalField(_('Сумма после налогов'),max_digits=15, decimal_places=2)
    sum_t = models.DecimalField(_('Сумма налога'),max_digits=15, decimal_places=2)

    date_payed = models.DateTimeField(_('Дата оплаты'), null=True, blank=True)
    is_payed = models.BooleanField(_('Может быть удален'),default=False, choices = BOOL_CHOICES_YESNO, null=True, blank=True)

    account = models.ForeignKey(Account, verbose_name='Аккаунт', on_delete=models.PROTECT)


    class Meta:
        ordering = ['name']

    def __str__(self):
        return str(self.name) + ' ' + str(self.period_start) + ' — ' + str(self.period_end)



class Emails(models.Model, IsDeletableMixin):
    welcome_is_sent = models.BooleanField(_('Welcome mail sent'),default=False, choices = BOOL_CHOICES_YESNO)
    welcome_date = models.DateTimeField(_('Welcome mail sent date'), null=True)

    trial_ends_10_is_sent = models.BooleanField(_('Trial ends 10 days'),default=False, choices = BOOL_CHOICES_YESNO)
    trial_ends_10_date = models.DateTimeField(_('Trial ends 10 days date'), null=True)

    trial_ends_1_is_sent = models.BooleanField(_('Trial ends 1 days'),default=False, choices = BOOL_CHOICES_YESNO)
    trial_ends_1_date = models.DateTimeField(_('Trial ends 1 days date'), null=True)

    account_blocked_is_sent = models.BooleanField(_('mail account blocked'),default=False, choices = BOOL_CHOICES_YESNO)
    account_blocked_date = models.DateTimeField(_('mail account blocked date'), null=True)

    account_unblocked_is_sent = models.BooleanField(_('mail account unblocked'),default=False, choices = BOOL_CHOICES_YESNO)
    account_unblocked_date = models.DateTimeField(_('mail account unblocked date'), null=True)

    account = models.OneToOneField(Account, verbose_name='Аккаунт', on_delete=models.CASCADE, primary_key=True,)


    class Meta:
        ordering = ['account']

    def __str__(self):
        return 'Статус писем для аккаунта №' + str(self.account)

class WordingSection(models.Model, IsDeletableMixin):
    name = models.CharField(_('Раздел'), max_length=200)

    class Meta:
        ordering = ['name']

    def __str__(self):
        return self.name

class WordingSubsection(models.Model, IsDeletableMixin):
    name = models.CharField(_('Подраздел'), max_length=200)
    section = models.ForeignKey(WordingSection, related_name='subsections', on_delete=models.PROTECT)

    class Meta:
        ordering = ['name']

    def __str__(self):
        return self.name

class WordingType(models.Model, IsDeletableMixin):
    name = models.CharField(_('Тип'), max_length=200)

    class Meta:
        ordering = ['name']

    def __str__(self):
        return self.name

class Wording(models.Model, IsDeletableMixin):
    code = models.IntegerField(_('Код текста'), unique=True)
    section = models.ForeignKey(WordingSection, verbose_name='Раздел', related_name='section_wordings', on_delete=models.PROTECT)
    subsection = models.ForeignKey(WordingSubsection, verbose_name='Подраздел', related_name='subsection_wordings', on_delete=models.PROTECT, null=True, blank=True)
    type = models.ForeignKey(WordingType, verbose_name='Тип', related_name='type_wordings', on_delete=models.PROTECT)
    text = models.CharField(_('Текст'), max_length=20000)

    class Meta:
        ordering = ['code']

    def __str__(self):
        return str(self.code)

