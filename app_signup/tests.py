from django.test import TestCase, Client, RequestFactory
from django.test import Client
from django.urls import reverse
from .forms import *
from app_signup import models as app_signup_models
from django.contrib.sites.models import Site
from subdomains.utils import reverse as sub_reverse
from django.conf import settings
from fins.threadlocal import thread_local
from app_signup import views as app_signup_views
from app_transaction import models as app_transaction_models
from fins.global_functions import get_db_name_test as get_db_name



class SignupTest(TestCase):

    def setUp(self):
        self.client = Client()
        site = Site.objects.get(pk=1)
        site.domain = 'testserver'
        site.save()
        self.factory = RequestFactory()


    def tearDown(self):

        #   we delete created account db
        connection_params = {
            'database': settings.MAIN_DB['NAME'],
            'host': settings.MAIN_DB['HOST'],
            'user': settings.MAIN_DB['USER'],
            'password': settings.MAIN_DB['PASSWORD']
        }
        connection = psycopg2.connect(**connection_params)
        connection.set_isolation_level(
            psycopg2.extensions.ISOLATION_LEVEL_AUTOCOMMIT
        )
        cursor = connection.cursor()

        for obj in app_signup_models.Account.objects.all():
            name = 'account_' + str(obj.pk)

            cursor.execute("SELECT pg_terminate_backend(pg_stat_activity.pid)"
                           " FROM pg_stat_activity"
                           " WHERE pg_stat_activity.datname = '%s' AND pid <> pg_backend_pid();" % name)
            cursor.execute('DROP DATABASE IF EXISTS "%s"' % name)
        connection.close()

    def test_signup_200(self):
        """
        check if page return 200
        """
        response = self.client.get(reverse('app_signup:signup'))
        self.assertEqual(response.status_code, 200)

    def test_signup_required(self):
        """
        check fields errors:
        - requied
        - unique

        check 200 response
        """
        response = self.client.post(reverse('app_signup:signup'),
                                    {
                                     'first_name': [''],
                                     'last_name': [''],
                                     'email': [''],
                                     'password1': [''],
                                     'password2': [''],
                                     'name': [''],
                                     'subdomain': ['']
                                    })
        self.assertFormError(response, 'user_form', 'first_name', 'Обязательное поле.')
        self.assertFormError(response, 'user_form', 'last_name', 'Обязательное поле.')
        self.assertFormError(response, 'user_form', 'email', 'Обязательное поле.')
        self.assertFormError(response, 'user_form', 'password1', 'Обязательное поле.')
        self.assertFormError(response, 'user_form', 'password2', 'Обязательное поле.')
        self.assertFormError(response, 'account_form', 'name', 'Обязательное поле.')
        self.assertFormError(response, 'account_form', 'subdomain', 'Обязательное поле.')

        Account.objects.create(subdomain = "testsub" )

        response = self.client.post(reverse('app_signup:signup'),
                                    {
                                     'first_name': [''],
                                     'last_name': [''],
                                     'email': [''],
                                     'password1': [''],
                                     'password2': [''],
                                     'name': [''],
                                     'subdomain': ['testsub']
                                    })
        self.assertFormError(response, 'account_form', 'subdomain', 'Субдомен уже занят')
        self.assertEqual(response.status_code, 200)

    def test_signup_redirect(self):
        """
        check redirect to subdomain compnany url after signup and user is authenticated
        check that account db are created and delete them

        """
        response = self.client.post(reverse('app_signup:signup'),
                                    {
                                     'first_name': ['test'],
                                     'last_name': ['test'],
                                     'email': ['test@test.ru'],
                                     'password1': ['qweqwe111'],
                                     'password2': ['qweqwe111'],
                                     'name': ['Company'],
                                     'subdomain': ['mysubdomain']
                                    }, follow=True)
        self.assertRedirects(response, sub_reverse(settings.LOGIN_REDIRECT_URL, subdomain='mysubdomain'),
                             status_code=302, target_status_code=200,
                             msg_prefix='', fetch_redirect_response=True)




        #check that account db are created and delete them
        connection_params = {
            'database': get_db_name(),
            'host': settings.MAIN_DB['HOST'],
            'user': settings.MAIN_DB['USER'],
            'password': settings.MAIN_DB['PASSWORD']
        }
        connection = psycopg2.connect(**connection_params)
        self.assertTrue(connection) # check if db was created
        connection.close()

    def test_signal(self):
        """
        check signal works - DBUSer created when User is created

        """
        response = self.client.post(reverse('app_signup:signup'),
                                    {
                                     'first_name': ['test'],
                                     'last_name': ['test'],
                                     'email': ['test@test.ru'],
                                     'password1': ['qweqwe111'],
                                     'password2': ['qweqwe111'],
                                     'name': ['Company'],
                                     'subdomain': ['mysubdomain']
                                    }, follow=True)
        user = app_signup_models.User.objects.get(email='test@test.ru')

        self.assertTrue(
            app_transaction_models.DBUser.objects.using(get_db_name()).get(
                user_id=user.id))  # check that signal works when user is created




