def access_default(item):

    item.access_app_dashboard = 2
    item.access_app_docs = 2
    item.access_app_setup = 0
    item.access_app_project = 2
    item.access_app_transaction = 2
    item.access_app_reports = 2
    item.access_users = 0
    item.access_can_block = 0

    return item