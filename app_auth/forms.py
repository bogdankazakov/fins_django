from django import forms

class LoginForm(forms.Form):
    username = forms.CharField(label='Ваш логн', max_length=100)
    password = forms.CharField(widget=forms.PasswordInput(), label='Ваш пароль', max_length=100)