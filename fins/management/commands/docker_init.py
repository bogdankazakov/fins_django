from django.core.management.base import BaseCommand, CommandError
from django.contrib.auth import get_user_model
from django.contrib.sites.models import Site
import os

class Command(BaseCommand):
    help = 'Run migrations for accounts databases'

    def handle(self, *args, **options):
        self.stdout.write('Starting docker_init...')

        User = get_user_model()
        if len(User.objects.filter(is_superuser=True)) == 0:
            User.objects.create_superuser(password='qweqwe111', email='admin@admin.ru')
            self.stdout.write('Superuser created.')

        try:
            site = Site.objects.get(domain='example.com')
            site.domain = os.environ.get("DOMAIN")
            site.name = os.environ.get("DOMAIN")
            site.save()
            self.stdout.write('Site edited.')
        except Exception:
            pass
        
        self.stdout.write('Docker_init finished.')