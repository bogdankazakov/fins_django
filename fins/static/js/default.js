$(function () {

         var addspinner = function (theform, btn) {
             var submit_btn;
             if (btn){
                 submit_btn = $("#" + btn)
             } else{
                 submit_btn = $("#" + theform.id + " :submit")
             }
             submit_btn.append(
                `<span class="spinner-grow spinner-grow-sm"  style="vertical-align: baseline; margin-bottom: -2px;"  role="status" aria-hidden="true"></span>`
             );
             submit_btn.prop("disabled", true)
         }


});