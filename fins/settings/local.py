from fins.settings.base import *
# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True
from  fins.db_mapper import DynamicDatabaseMapDBServer

# Database
# https://docs.djangoproject.com/en/2.1/ref/settings/#databases
#
MAIN_DB = {
    'ENGINE': 'django.db.backends.postgresql_psycopg2',
    'NAME': 'fins_test',
    'USER': 'bigtime',
    'PASSWORD': 'anacrisanacris',
    'HOST': 'localhost',
    'PORT': '',
}

ACCOUNTS_DB = {
    'ENGINE': 'django.db.backends.postgresql_psycopg2',
    'NAME': 'fins_test',
    'USER': 'bigtime',
    'PASSWORD': 'anacrisanacris',
    'HOST': 'localhost',
    'PORT': '',
}

INTERNAL_IPS = [
    '127.0.0.1',
]

DATABASES = DynamicDatabaseMapDBServer(MAIN_DB, ACCOUNTS_DB)

DATABASE_ROUTERS = ['fins.routers.TenantRouter']

SESSION_COOKIE_DOMAIN='.fins.my'
EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'



MEDIA_ROOT = os.path.join(BASE_DIR, 'media')
MEDIA_URL = '/media/'