from app_signup.models import Account
from .threadlocal import *

def tenant_middleware(get_response):
    # One-time configuration and initialization.

    def middleware(request):
        # Code to be executed for each request before
        # the view (and later middleware) are called.

        try:
            account = Account.objects.get(subdomain=request.subdomain)
            db = 'account_' + str(account.pk)
            @thread_local(using_db=db)
            def execute_request(request):
                return get_response(request)

        except Account.DoesNotExist:
            account = None

            @thread_local(using_db='default')
            def execute_request(request):
                return get_response(request)
        request.account = account

        response = execute_request(request)


        # Code to be executed for each request/response after
        # the view is called.

        return response

    return middleware