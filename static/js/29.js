(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[29],{

/***/ "./node_modules/cache-loader/dist/cjs.js?!./node_modules/babel-loader/lib/index.js!./node_modules/cache-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./src/views/Reports/Reports.vue?vue&type=script&lang=js&":
/*!*******************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/cache-loader/dist/cjs.js??ref--12-0!./node_modules/babel-loader/lib!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/views/Reports/Reports.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _wording_json__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./wording.json */ \"./src/views/Reports/wording.json\");\nvar _wording_json__WEBPACK_IMPORTED_MODULE_0___namespace = /*#__PURE__*/__webpack_require__.t(/*! ./wording.json */ \"./src/views/Reports/wording.json\", 1);\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n\n/* harmony default export */ __webpack_exports__[\"default\"] = ({\n  name: 'Reports',\n  computed: {\n    wording: function wording() {\n      if (this.$store.getters.LANGUAGE === 'ru') {\n        return _wording_json__WEBPACK_IMPORTED_MODULE_0__.ru;\n      } else if (this.$store.getters.LANGUAGE === 'en') {\n        return _wording_json__WEBPACK_IMPORTED_MODULE_0__.en;\n      }\n    }\n  }\n});\n\n//# sourceURL=webpack:///./src/views/Reports/Reports.vue?./node_modules/cache-loader/dist/cjs.js??ref--12-0!./node_modules/babel-loader/lib!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options");

/***/ }),

/***/ "./node_modules/cache-loader/dist/cjs.js?{\"cacheDirectory\":\"node_modules/.cache/vue-loader\",\"cacheIdentifier\":\"10c04e98-vue-loader-template\"}!./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/cache-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./src/views/Reports/Reports.vue?vue&type=template&id=318f6285&":
/*!***************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/cache-loader/dist/cjs.js?{"cacheDirectory":"node_modules/.cache/vue-loader","cacheIdentifier":"10c04e98-vue-loader-template"}!./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/views/Reports/Reports.vue?vue&type=template&id=318f6285& ***!
  \***************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"render\", function() { return render; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"staticRenderFns\", function() { return staticRenderFns; });\nvar render = function() {\n  var _vm = this\n  var _h = _vm.$createElement\n  var _c = _vm._self._c || _h\n  return _c(\"div\", [\n    _c(\"section\", { staticClass: \"header \" }, [\n      _c(\"div\", { staticStyle: { \"white-space\": \"nowrap\" } }, [\n        _c(\"h1\", { staticClass: \"title\" }, [\n          _vm._v(\" \" + _vm._s(_vm.wording.header) + \" \")\n        ])\n      ])\n    ]),\n    _c(\"section\", { staticClass: \"content-area\" }, [\n      _c(\"div\", { staticClass: \"block-section-3\" }, [\n        _c(\"div\", { staticClass: \"row mb-5 mt-3\" }, [\n          _c(\"div\", { staticClass: \"col-4\" }, [\n            _c(\"h2\", [_vm._v(_vm._s(_vm.wording.subheader.classic))])\n          ]),\n          _c(\"div\", { staticClass: \"col-4\" }, [\n            _c(\"h2\", [_vm._v(_vm._s(_vm.wording.subheader.effectiveness))])\n          ])\n        ]),\n        _c(\"div\", { staticClass: \"row\" }, [\n          _c(\"div\", { staticClass: \"col-4\" }, [\n            _c(\n              \"p\",\n              [\n                _c(\"router-link\", { attrs: { to: { name: \"ReportPL\" } } }, [\n                  _vm._v(_vm._s(_vm.wording.menu.pl))\n                ])\n              ],\n              1\n            ),\n            _c(\"hr\"),\n            _c(\n              \"p\",\n              [\n                _c(\n                  \"router-link\",\n                  { attrs: { to: { name: \"ReportCashflow\" } } },\n                  [_vm._v(_vm._s(_vm.wording.menu.cashflow))]\n                ),\n                _c(\"br\")\n              ],\n              1\n            )\n          ]),\n          _c(\"div\", { staticClass: \"col-4\" }, [\n            _c(\n              \"p\",\n              [\n                _c(\n                  \"router-link\",\n                  { attrs: { to: { name: \"ReportSubcontractors\" } } },\n                  [_vm._v(_vm._s(_vm.wording.menu.subcontractors))]\n                )\n              ],\n              1\n            ),\n            _c(\"hr\"),\n            _c(\n              \"p\",\n              [\n                _c(\"router-link\", { attrs: { to: { name: \"ReportBrands\" } } }, [\n                  _vm._v(_vm._s(_vm.wording.menu.brands))\n                ])\n              ],\n              1\n            )\n          ])\n        ])\n      ])\n    ])\n  ])\n}\nvar staticRenderFns = []\nrender._withStripped = true\n\n\n\n//# sourceURL=webpack:///./src/views/Reports/Reports.vue?./node_modules/cache-loader/dist/cjs.js?%7B%22cacheDirectory%22:%22node_modules/.cache/vue-loader%22,%22cacheIdentifier%22:%2210c04e98-vue-loader-template%22%7D!./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options");

/***/ }),

/***/ "./src/views/Reports/Reports.vue":
/*!***************************************!*\
  !*** ./src/views/Reports/Reports.vue ***!
  \***************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _Reports_vue_vue_type_template_id_318f6285___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Reports.vue?vue&type=template&id=318f6285& */ \"./src/views/Reports/Reports.vue?vue&type=template&id=318f6285&\");\n/* harmony import */ var _Reports_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Reports.vue?vue&type=script&lang=js& */ \"./src/views/Reports/Reports.vue?vue&type=script&lang=js&\");\n/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ \"./node_modules/vue-loader/lib/runtime/componentNormalizer.js\");\n\n\n\n\n\n/* normalize component */\n\nvar component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__[\"default\"])(\n  _Reports_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__[\"default\"],\n  _Reports_vue_vue_type_template_id_318f6285___WEBPACK_IMPORTED_MODULE_0__[\"render\"],\n  _Reports_vue_vue_type_template_id_318f6285___WEBPACK_IMPORTED_MODULE_0__[\"staticRenderFns\"],\n  false,\n  null,\n  null,\n  null\n  \n)\n\n/* hot reload */\nif (false) { var api; }\ncomponent.options.__file = \"src/views/Reports/Reports.vue\"\n/* harmony default export */ __webpack_exports__[\"default\"] = (component.exports);\n\n//# sourceURL=webpack:///./src/views/Reports/Reports.vue?");

/***/ }),

/***/ "./src/views/Reports/Reports.vue?vue&type=script&lang=js&":
/*!****************************************************************!*\
  !*** ./src/views/Reports/Reports.vue?vue&type=script&lang=js& ***!
  \****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _node_modules_cache_loader_dist_cjs_js_ref_12_0_node_modules_babel_loader_lib_index_js_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Reports_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/cache-loader/dist/cjs.js??ref--12-0!../../../node_modules/babel-loader/lib!../../../node_modules/cache-loader/dist/cjs.js??ref--0-0!../../../node_modules/vue-loader/lib??vue-loader-options!./Reports.vue?vue&type=script&lang=js& */ \"./node_modules/cache-loader/dist/cjs.js?!./node_modules/babel-loader/lib/index.js!./node_modules/cache-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./src/views/Reports/Reports.vue?vue&type=script&lang=js&\");\n/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__[\"default\"] = (_node_modules_cache_loader_dist_cjs_js_ref_12_0_node_modules_babel_loader_lib_index_js_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Reports_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__[\"default\"]); \n\n//# sourceURL=webpack:///./src/views/Reports/Reports.vue?");

/***/ }),

/***/ "./src/views/Reports/Reports.vue?vue&type=template&id=318f6285&":
/*!**********************************************************************!*\
  !*** ./src/views/Reports/Reports.vue?vue&type=template&id=318f6285& ***!
  \**********************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _node_modules_cache_loader_dist_cjs_js_cacheDirectory_node_modules_cache_vue_loader_cacheIdentifier_10c04e98_vue_loader_template_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Reports_vue_vue_type_template_id_318f6285___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/cache-loader/dist/cjs.js?{\"cacheDirectory\":\"node_modules/.cache/vue-loader\",\"cacheIdentifier\":\"10c04e98-vue-loader-template\"}!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/cache-loader/dist/cjs.js??ref--0-0!../../../node_modules/vue-loader/lib??vue-loader-options!./Reports.vue?vue&type=template&id=318f6285& */ \"./node_modules/cache-loader/dist/cjs.js?{\\\"cacheDirectory\\\":\\\"node_modules/.cache/vue-loader\\\",\\\"cacheIdentifier\\\":\\\"10c04e98-vue-loader-template\\\"}!./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/cache-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./src/views/Reports/Reports.vue?vue&type=template&id=318f6285&\");\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"render\", function() { return _node_modules_cache_loader_dist_cjs_js_cacheDirectory_node_modules_cache_vue_loader_cacheIdentifier_10c04e98_vue_loader_template_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Reports_vue_vue_type_template_id_318f6285___WEBPACK_IMPORTED_MODULE_0__[\"render\"]; });\n\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"staticRenderFns\", function() { return _node_modules_cache_loader_dist_cjs_js_cacheDirectory_node_modules_cache_vue_loader_cacheIdentifier_10c04e98_vue_loader_template_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Reports_vue_vue_type_template_id_318f6285___WEBPACK_IMPORTED_MODULE_0__[\"staticRenderFns\"]; });\n\n\n\n//# sourceURL=webpack:///./src/views/Reports/Reports.vue?");

/***/ }),

/***/ "./src/views/Reports/wording.json":
/*!****************************************!*\
  !*** ./src/views/Reports/wording.json ***!
  \****************************************/
/*! exports provided: ru, en, default */
/***/ (function(module) {

eval("module.exports = JSON.parse(\"{\\\"ru\\\":{\\\"header\\\":\\\"Отчеты\\\",\\\"subheader\\\":{\\\"classic\\\":\\\"Классические отчеты\\\",\\\"effectivness\\\":\\\"Эффективность\\\"},\\\"menu\\\":{\\\"pl\\\":\\\"Отчет о прибыли и убытках (P&L)\\\",\\\"cashflow\\\":\\\"Отчет о ДДС (Cashflow)\\\",\\\"subcontractors\\\":\\\"Отчет по контрагентам\\\",\\\"brands\\\":\\\"Отчет по брендам\\\"}},\\\"en\\\":{\\\"header\\\":\\\"Reports\\\",\\\"subheader\\\":{\\\"classic\\\":\\\"Classic reports\\\",\\\"effectiveness\\\":\\\"Effectiveness\\\"},\\\"menu\\\":{\\\"pl\\\":\\\"P&L report\\\",\\\"cashflow\\\":\\\"Cashflow report\\\",\\\"subcontractors\\\":\\\"Subcontractors report\\\",\\\"brands\\\":\\\"Brand report\\\"}}}\");\n\n//# sourceURL=webpack:///./src/views/Reports/wording.json?");

/***/ })

}]);