$(function () {

  var loadForm = function () {
    var btn = $(this);
    $.ajax({
      url: btn.attr("data-url"),
      type: 'get',
      dataType: 'json',
      beforeSend: function () {
        $("#modal-global").modal("show");
      },
      success: function (data) {
        $("#modal-global .modal-content").html(data.html_form);
      }
    });
  };

  var saveForm = function () {
    var form = $(this);
    var formData = new FormData(form[0]);
    console.log(formData)
    $.ajax({
      url: form.attr("action"),
      data: formData,
      type: form.attr("method"),
      dataType: 'json',
      async: true,
      cache: false,
      contentType: false,
      enctype: form.attr("enctype"),
      processData: false,
      success: function (data) {
        if (data.form_is_valid) {
          $("#doc_templates_list tbody").html(data.html_list);
          $("#modal-global").modal("hide");
        }
        else {
          $("#modal-global .modal-content").html(data.html_form);
        }
      },
      error: function () {
          console.log("error")
        }
    });
    return false;
  };


  // Upload template
  $(".js-create-doc_template").click(loadForm);
  $("#modal-global").on("submit", ".js-doc_template-create-form", saveForm);

  // Update template
  $("#doc_templates_list").on("click", ".js-update-doc_template", loadForm);
  $("#modal-global").on("submit", ".js-doc_template-update-form", saveForm);

  // Delete template
  $("#doc_templates_list").on("click", ".js-delete-doc_template", loadForm);
  $("#modal-global").on("submit", ".js-doc_template-delete-form", saveForm);


  var loadList = function () {
    var btn = $(this);
    $.ajax({
      url: btn.attr("data-url"),
      type: 'get',
      dataType: 'json',
      beforeSend: function () {
        $("#modal-global").modal("show");
      },
      success: function (data) {
        $("#modal-global .modal-content").html(data.html_form);
      }
    });
  };

    // Show history
  $(".js-history-doc_template").click(loadList);

});