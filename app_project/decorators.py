from django.contrib.auth import REDIRECT_FIELD_NAME
from django.contrib.auth.decorators import user_passes_test
from django.shortcuts import render, redirect
from subdomains.utils import reverse as sub_reverse
from django.urls import reverse
from functools import wraps
from app_signup.models import User
from django.http import HttpResponseRedirect
from django.conf import settings
from app_project import models as app_project_models
from app_setup import models as app_setup_models


def access_app_project_section(function):
    @wraps(function)
    def wrap(request, *args, **kwargs):
        '''
        Checks if user has access to Project section and let in
        '''

        dbuser = app_setup_models.DBUser.objects.get(user_id=request.user.id)
        access_level = dbuser.access_app_project
        if access_level == 0:
            return redirect(sub_reverse(settings.ACCESS_DENIED_TO_APP_REDIRECT_URL, subdomain=request.account.subdomain))
        else:
            return function(request, *args, **kwargs)

    return wrap

def access_app_projects_list(function):
    @wraps(function)
    def wrap(request, *args, **kwargs):
        '''
        Checks if user has access to Project section and returns get all function
        '''

        dbuser = app_setup_models.DBUser.objects.get(user_id=request.user.id)
        access_level = dbuser.access_app_project
        if access_level == 0:
            return redirect(sub_reverse(settings.ACCESS_DENIED_TO_APP_REDIRECT_URL, subdomain=request.account.subdomain))

        elif access_level == 1:
            def get_access_all():
                all = app_project_models.Project.objects.prefetch_related(
                    'project_products',
                ).filter(teamprojects_in_project__user=dbuser).order_by('-pk')
                return all
            return function(request, get_access_all, *args, **kwargs)

        elif access_level == 3:
            def get_access_all():
                all = app_project_models.Project.objects.prefetch_related(
                    'project_products',
                ).filter(teamprojects_in_project__user__unit=dbuser.unit).order_by('-pk')
                return all
            return function(request, get_access_all, *args, **kwargs)

        else:
            def get_access_all():
                all = app_project_models.Project.objects.prefetch_related(
                    'project_products',
                ).all().order_by('-pk')
                return all
            return function(request, get_access_all, *args, **kwargs)

    return wrap

def access_app_project_card(function):
    @wraps(function)
    def wrap(request, *args, **kwargs):
        '''
        Checks if user has access to the project
        '''
        pk = kwargs['pk']

        dbuser = app_setup_models.DBUser.objects.get(user_id=request.user.id)
        access_level = dbuser.access_app_project

        if access_level == 0:
            return redirect(sub_reverse(settings.ACCESS_DENIED_TO_APP_REDIRECT_URL, subdomain=request.account.subdomain))

        elif access_level == 1:
            all = app_project_models.Project.objects.filter(teamprojects_in_project__user=dbuser).order_by('-pk').values_list('pk', flat=True)
            all = list(all)
            if int(pk) in all:
                return function(request, *args, **kwargs)
            else:
                return redirect(sub_reverse(settings.ACCESS_DENIED_TO_APP_REDIRECT_URL, subdomain=request.account.subdomain))


        elif access_level == 3:
            all = app_project_models.Project.objects.filter(teamprojects_in_project__user__unit=dbuser.unit).order_by(
                '-pk').values_list('pk', flat=True)
            all = list(all)
            if int(pk) in all:
                return function(request, *args, **kwargs)
            else:
                return redirect(sub_reverse(settings.ACCESS_DENIED_TO_APP_REDIRECT_URL, subdomain=request.account.subdomain))

        else:
            return function(request, *args, **kwargs)

    return wrap

def access_app_project_services_C(function):
    @wraps(function)
    def wrap(request, *args, **kwargs):
        '''
        Checks if user has access to the project
        '''
        pk = kwargs['projectpk']

        dbuser = app_setup_models.DBUser.objects.get(user_id=request.user.id)
        access_level = dbuser.access_app_project

        if access_level == 0:
            return redirect(sub_reverse(settings.ACCESS_DENIED_TO_APP_REDIRECT_URL, subdomain=request.account.subdomain))

        elif access_level == 1:
            all = app_project_models.Project.objects.filter(teamprojects_in_project__user=dbuser).order_by('-pk').values_list('pk', flat=True)
            all = list(all)
            if int(pk) in all:
                return function(request, *args, **kwargs)
            else:
                return redirect(sub_reverse(settings.ACCESS_DENIED_TO_APP_REDIRECT_URL, subdomain=request.account.subdomain))


        elif access_level == 3:
            all = app_project_models.Project.objects.filter(teamprojects_in_project__user__unit=dbuser.unit).order_by(
                '-pk').values_list('pk', flat=True)
            all = list(all)
            if int(pk) in all:
                return function(request, *args, **kwargs)
            else:
                return redirect(sub_reverse(settings.ACCESS_DENIED_TO_APP_REDIRECT_URL, subdomain=request.account.subdomain))

        else:
            return function(request, *args, **kwargs)

    return wrap

def access_app_project_services_RUD(function):
    @wraps(function)
    def wrap(request, *args, **kwargs):
        '''
        Checks if user has access to the project
        '''
        pk = kwargs['pk']

        dbuser = app_setup_models.DBUser.objects.get(user_id=request.user.id)
        access_level = dbuser.access_app_project

        if access_level == 0:
            return redirect(sub_reverse(settings.ACCESS_DENIED_TO_APP_REDIRECT_URL, subdomain=request.account.subdomain))

        elif access_level == 1:
            all = app_project_models.Service.objects.filter(project__teamprojects_in_project__user=dbuser).order_by('-pk').values_list('pk', flat=True)
            all = list(all)
            if int(pk) in all:
                return function(request, *args, **kwargs)
            else:
                return redirect(sub_reverse(settings.ACCESS_DENIED_TO_APP_REDIRECT_URL, subdomain=request.account.subdomain))


        elif access_level == 3:
            all = app_project_models.Service.objects.filter(project__teamprojects_in_project__user__unit=dbuser.unit).order_by(
                '-pk').values_list('pk', flat=True)
            all = list(all)
            if int(pk) in all:
                return function(request, *args, **kwargs)
            else:
                return redirect(sub_reverse(settings.ACCESS_DENIED_TO_APP_REDIRECT_URL, subdomain=request.account.subdomain))

        else:
            return function(request, *args, **kwargs)

    return wrap


def access_app_project_periods_C(function):
    @wraps(function)
    def wrap(request, *args, **kwargs):
        '''
        Checks if user has access to the project
        '''
        pk = kwargs['projectpk']

        dbuser = app_setup_models.DBUser.objects.get(user_id=request.user.id)
        access_level = dbuser.access_app_project

        if access_level == 0:
            return redirect(sub_reverse(settings.ACCESS_DENIED_TO_APP_REDIRECT_URL, subdomain=request.account.subdomain))

        elif access_level == 1:
            all = app_project_models.Project.objects.filter(teamprojects_in_project__user=dbuser).order_by('-pk').values_list('pk', flat=True)
            all = list(all)
            if int(pk) in all:
                return function(request, *args, **kwargs)
            else:
                return redirect(sub_reverse(settings.ACCESS_DENIED_TO_APP_REDIRECT_URL, subdomain=request.account.subdomain))


        elif access_level == 3:
            all = app_project_models.Project.objects.filter(teamprojects_in_project__user__unit=dbuser.unit).order_by(
                '-pk').values_list('pk', flat=True)
            all = list(all)
            if int(pk) in all:
                return function(request, *args, **kwargs)
            else:
                return redirect(sub_reverse(settings.ACCESS_DENIED_TO_APP_REDIRECT_URL, subdomain=request.account.subdomain))

        else:
            return function(request, *args, **kwargs)

    return wrap

def access_app_project_periods_RUD(function):
    @wraps(function)
    def wrap(request, *args, **kwargs):
        '''
        Checks if user has access to the project
        '''
        pk = kwargs['pk']

        dbuser = app_setup_models.DBUser.objects.get(user_id=request.user.id)
        access_level = dbuser.access_app_project

        if access_level == 0:
            return redirect(sub_reverse(settings.ACCESS_DENIED_TO_APP_REDIRECT_URL, subdomain=request.account.subdomain))

        elif access_level == 1:
            all = app_project_models.ReportPeriod.objects.filter(project__teamprojects_in_project__user=dbuser).order_by('-pk').values_list('pk', flat=True)
            all = list(all)
            if int(pk) in all:
                return function(request, *args, **kwargs)
            else:
                return redirect(sub_reverse(settings.ACCESS_DENIED_TO_APP_REDIRECT_URL, subdomain=request.account.subdomain))


        elif access_level == 3:
            all = app_project_models.ReportPeriod.objects.filter(project__teamprojects_in_project__user__unit=dbuser.unit).order_by(
                '-pk').values_list('pk', flat=True)
            all = list(all)
            if int(pk) in all:
                return function(request, *args, **kwargs)
            else:
                return redirect(sub_reverse(settings.ACCESS_DENIED_TO_APP_REDIRECT_URL, subdomain=request.account.subdomain))

        else:
            return function(request, *args, **kwargs)

    return wrap


def access_app_project_team_C(function):
    @wraps(function)
    def wrap(request, *args, **kwargs):
        '''
        Checks if user has access to the project
        '''
        pk = kwargs['projectpk']

        dbuser = app_setup_models.DBUser.objects.get(user_id=request.user.id)
        access_level = dbuser.access_app_project

        if access_level == 0:
            return redirect(sub_reverse(settings.ACCESS_DENIED_TO_APP_REDIRECT_URL, subdomain=request.account.subdomain))

        elif access_level == 1:
            all = app_project_models.Project.objects.filter(teamprojects_in_project__user=dbuser).order_by('-pk').values_list('pk', flat=True)
            all = list(all)
            if int(pk) in all:
                return function(request, *args, **kwargs)
            else:
                return redirect(sub_reverse(settings.ACCESS_DENIED_TO_APP_REDIRECT_URL, subdomain=request.account.subdomain))


        elif access_level == 3:
            all = app_project_models.Project.objects.filter(teamprojects_in_project__user__unit=dbuser.unit).order_by(
                '-pk').values_list('pk', flat=True)
            all = list(all)
            if int(pk) in all:
                return function(request, *args, **kwargs)
            else:
                return redirect(sub_reverse(settings.ACCESS_DENIED_TO_APP_REDIRECT_URL, subdomain=request.account.subdomain))

        else:
            return function(request, *args, **kwargs)

    return wrap

def access_app_project_team_RUD(function):
    @wraps(function)
    def wrap(request, *args, **kwargs):
        '''
        Checks if user has access to the project
        '''
        pk = kwargs['pk']

        dbuser = app_setup_models.DBUser.objects.get(user_id=request.user.id)
        access_level = dbuser.access_app_project

        if access_level == 0:
            return redirect(sub_reverse(settings.ACCESS_DENIED_TO_APP_REDIRECT_URL, subdomain=request.account.subdomain))

        elif access_level == 1:
            all = app_project_models.TeamProject.objects.filter(project__teamprojects_in_project__user=dbuser).order_by('-pk').values_list('pk', flat=True)
            all = list(all)
            if int(pk) in all:
                return function(request, *args, **kwargs)
            else:
                return redirect(sub_reverse(settings.ACCESS_DENIED_TO_APP_REDIRECT_URL, subdomain=request.account.subdomain))


        elif access_level == 3:
            all = app_project_models.TeamProject.objects.filter(project__teamprojects_in_project__user__unit=dbuser.unit).order_by(
                '-pk').values_list('pk', flat=True)
            all = list(all)
            if int(pk) in all:
                return function(request, *args, **kwargs)
            else:
                return redirect(sub_reverse(settings.ACCESS_DENIED_TO_APP_REDIRECT_URL, subdomain=request.account.subdomain))

        else:
            return function(request, *args, **kwargs)

    return wrap



def access_app_project_reports_C(function):
    @wraps(function)
    def wrap(request, *args, **kwargs):
        '''
        Checks if user has access to the project
        '''
        pk = kwargs['projectpk']

        dbuser = app_setup_models.DBUser.objects.get(user_id=request.user.id)
        access_level = dbuser.access_app_project

        if access_level == 0:
            return redirect(sub_reverse(settings.ACCESS_DENIED_TO_APP_REDIRECT_URL, subdomain=request.account.subdomain))

        elif access_level == 1:
            all = app_project_models.Project.objects.filter(teamprojects_in_project__user=dbuser).order_by('-pk').values_list('pk', flat=True)
            all = list(all)
            if int(pk) in all:
                return function(request, *args, **kwargs)
            else:
                return redirect(sub_reverse(settings.ACCESS_DENIED_TO_APP_REDIRECT_URL, subdomain=request.account.subdomain))


        elif access_level == 3:
            all = app_project_models.Project.objects.filter(teamprojects_in_project__user__unit=dbuser.unit).order_by(
                '-pk').values_list('pk', flat=True)
            all = list(all)
            if int(pk) in all:
                return function(request, *args, **kwargs)
            else:
                return redirect(sub_reverse(settings.ACCESS_DENIED_TO_APP_REDIRECT_URL, subdomain=request.account.subdomain))

        else:
            return function(request, *args, **kwargs)

    return wrap

def access_app_project_reports_RUD(function):
    @wraps(function)
    def wrap(request, *args, **kwargs):
        '''
        Checks if user has access to the project
        '''
        pk = kwargs['pk']

        dbuser = app_setup_models.DBUser.objects.get(user_id=request.user.id)
        access_level = dbuser.access_app_project

        if access_level == 0:
            return redirect(sub_reverse(settings.ACCESS_DENIED_TO_APP_REDIRECT_URL, subdomain=request.account.subdomain))

        elif access_level == 1:
            all = app_project_models.Report.objects.filter(period__project__teamprojects_in_project__user=dbuser).order_by('-pk').values_list('pk', flat=True)
            all = list(all)
            if int(pk) in all:
                return function(request, *args, **kwargs)
            else:
                return redirect(sub_reverse(settings.ACCESS_DENIED_TO_APP_REDIRECT_URL, subdomain=request.account.subdomain))


        elif access_level == 3:
            all = app_project_models.Report.objects.filter(period__project__teamprojects_in_project__user__unit=dbuser.unit).order_by(
                '-pk').values_list('pk', flat=True)
            all = list(all)
            if int(pk) in all:
                return function(request, *args, **kwargs)
            else:
                return redirect(sub_reverse(settings.ACCESS_DENIED_TO_APP_REDIRECT_URL, subdomain=request.account.subdomain))

        else:
            return function(request, *args, **kwargs)

    return wrap