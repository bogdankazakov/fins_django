from app_project.decorators import *
from app_auth.decorators import access_company_level
from django.contrib.auth.decorators import login_required
from app_project import models as app_project_models
from app_project.forms import ReportForm, ServiceInReportFormSet, ServiceInReportUpdateFormSet
from app_project.tools import report_add_data, period_tool, serviceinreport_killer, can_delete_report
from app_setup.tools import log_data
from django.template.loader import render_to_string
from django.http import JsonResponse
from django.contrib import messages
from app_project.views.main import wording
from app_transaction import models as app_transaction_models

@login_required
@access_company_level
@access_app_project_reports_C
def report_create(request, projectpk, periodpk):
    project = app_project_models.Project.objects.get(pk=projectpk)
    services = app_project_models.Service.objects.filter(project=project).order_by('name')
    initial = []
    for service in services:
        initial.append({
            'service': service.pk,
        })

    if request.method == 'POST':
        form = ReportForm(request.POST)
        formset = ServiceInReportFormSet(request.POST)

    else:
        form = ReportForm()
        formset = ServiceInReportFormSet(initial=initial)
        formset = report_add_data(formset, services)
    return report_save(request, form, formset, 'app_project/report_CRUD/partial_create_report.html', projectpk, periodpk)

@login_required
@access_company_level
@access_app_project_reports_RUD
def report_update(request, pk, projectpk):
    item = app_project_models.Report.objects.get(pk=pk)
    periodpk = item.period.pk

    servicesIR = app_project_models.ServiceInReport.objects.filter(report=item)
    initial = []
    for service in servicesIR:
        initial.append({
            'service': service.service,
            'hours': service.hours,
        })
    services = app_project_models.Service.objects.filter(project_id=projectpk).order_by('name')



    if request.method == 'POST':
        form = ReportForm(request.POST, instance=item)
        formset = ServiceInReportUpdateFormSet(request.POST, queryset=servicesIR)
    else:
        form = ReportForm(instance=item)
        formset = ServiceInReportUpdateFormSet(queryset=servicesIR)
        formset = report_add_data(formset, services)


    return report_save(request, form, formset, 'app_project/report_CRUD/partial_update_report.html', projectpk, periodpk)

@login_required
@access_company_level
@access_app_project_section
def report_save(request, form, formset, template_name, projectpk, periodpk):
    project = app_project_models.Project.objects.get(pk=projectpk)
    period = app_project_models.ReportPeriod.objects.get(pk=periodpk)
    data = dict()
    if request.method == 'POST':
        if form.is_valid():

            report = form.save(commit=False)
            log_data(report, request)
            report.save()
            report.period = period
            report.save()

            data['form_is_valid'] = True

            context = {
                'periods': period_tool(project),
                'wording': wording,
                'project': project,
                'obj': app_transaction_models.Transaction,

            }
            data['html_list'] = render_to_string('app_project/project_card/partial_list_periods.html', context)
        else:
            messages.error(request, wording['validation_error'])
        for form in formset:
            if form.is_valid():
                if form.cleaned_data['hours'] != 0:
                    instance = form.save(commit=False)
                    instance.report = report
                    instance.save()

    if request.method == 'GET':
        data['form_is_valid'] = False
    context = {
            'form': form,
            'formset':formset,
            'wording': wording,
            'projectpk': projectpk,
            'periodpk': periodpk,
           }
    data['html_form'] = render_to_string(template_name, context, request=request)
    return JsonResponse(data)

@login_required
@access_company_level
@access_app_project_reports_RUD
def report_delete(request, pk):
    data = dict()
    item = app_project_models.Report.objects.get(pk=pk)
    project = item.period.project
    if request.method == 'POST':
        serviceinreport_killer(item)
        item.delete()
        data['form_is_valid'] = True
        messages.success(request, 'Отчет {0} удален'.format(item))

        context = {
            'periods': period_tool(project),
            'wording': wording,
            'project': project,
            'obj': app_transaction_models.Transaction,

        }
        data['html_list'] = render_to_string('app_project/project_card/partial_list_periods.html', context)

    if request.method == 'GET':


        deletable, related_obj = can_delete_report(item)


        context = {
            'item': item,
            'deletable':deletable,
            'wording': wording,
        }
        data['html_form'] = render_to_string('app_project/report_CRUD/partial_delete_report.html',context,request=request,)
    return JsonResponse(data)



